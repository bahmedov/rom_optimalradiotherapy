function [output] = optimal_solver(par)
% Solver
%    Solves optimality system minimization functional of deviation of 
%  an actual dose (solution of Boltzmann equation) from desired one. 
%  Angular dependece is approximated using Fourier expansion with Legendre Polyno-
%  mials. Then it is solved using Finite Elements Method (FEM). 
%  Optimality system is constructed as matrix and solved using Matlab
%  functions. 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 22.12.2014 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |
%% ========================================================================
% Description
%========================================================================
% Optimality system can be written as system of equations:
%
% |  M       0          0         0      -S  |  | Psi_0   |  | 0         |
% |1/3*D1  Sigma_t1     0         0       0  |  | Psi_1   |  | 0         |
% |a/2*S   3a/4*S       M         0       0  |* | Lambda_0|= |a/2*S*Psi_d| 
% | 0        0      -1/3*D1   Sigma_t1    0  |  | Lambda_1|  | 0         |   
% | 0        0        1/2        3/4      1  |  | Q       |  | 0         |
%
% For given desired particle distribution Psi_d it is necessary to find 
%   - Q  optimal source distribution
%
% Boltzmann Equation
% mu*d(psi) / dx + sigma_t*psi = 
%   sigma_s*Int (s(x,mu*mu')psi(mu'))d(mu') + q(x)    
%
%
%% ========================================================================
% Parameter Defaults
%========================================================================
if ~isfield(par,'name'),     par.name = 'Output'; end
if ~isfield(par,'arr_p'),    par.arr_p = [0.2 0.1 0.7 0.15]; end
if ~isfield(par,'arr_bp'),   par.arr_bp = [0.25 0.1 0.65 0.15]; end
if ~isfield(par,'ic'),       par.ic = @zero; end % default i.c.
if ~isfield(par,'sigma_a'),  par.sigma_a = @zero; end
if ~isfield(par,'sigma_s0'), par.sigma_s0 = @zero; end
if ~isfield(par,'sigma_sm'), par.sigma_sm = @zero; end
if ~isfield(par,'source'),   par.source = @zero; end % default source
if ~isfield(par,'ax'),       par.ax = [0 1]; end % size of domain
if ~isfield(par,'n'),        par.n = 100; end % number of disc. points
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'mom_output'),par.mom_output = 1; end % Moments plotted.
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'dose'),     par.dose = @default_dose; end %default des.dose
if ~isfield(par,'alpha'),    par.alpha = 1; end % alpha in min.functional 
if ~isfield(par,'FE'), 
    cprintf('*Magenta','Please, load FE metrices\n'); 
    return;
end 
% flags
if ~isfield(par,'flag'),     par.flag = init_flag(par); end
if ~isfield(par.flag,'ifplot'),   par.flag.ifplot = 0; end
if ~isfield(par.flag,'half_plot'), par.flag.half_plot = 0; end % not half-plot
if ~isfield(par.flag,'evaLB'),    par.flag.evaLB = 0; end % evaluate Lower Bound of a(*,*,p)
if ~isfield(par.flag,'evaLB_KKT'),    par.flag.evaLB_KKT = 0; end % evaluate Lower Bound of KKT system 
if ~isfield(par.flag,'evaUB'),    par.flag.evaUB = 0; end % evaluate Upper bound of b(*,*,p)
if ~isfield(par.flag,'evaLUB'),    par.flag.evaLUB = 0; end % evaluate Upper bound of L(*)
if ~isfield(par.flag,'evaRes'),   par.flag.evaRes = 0; end % evaiate Residual
if ~isfield(par.flag,'alt_control'),   par.flag.alt_control = 0; end % evaluate control EB
if ~isfield(par.flag,'alt_all_EB'),   par.flag.alt_all_EB = 0; end % evaiate all EB
if ~isfield(par.flag,'material'), par.flag.material = 0; end % material function/constant
%% ========================================================================
% Initialization and loading the metrices
%========================================================================
tic
cputime = zeros(1,4);
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
Br = par.FE.Br; 
multS = par.FE.multS;
multsT1 = par.FE.multsT1;
multsT2 = par.FE.multsT2;
D1 = par.FE.D1;
D2 = par.FE.D2;
Q{1} = par.source(par.x,0); % only zero-th moment of source is active
Q{2} = par.source(par.x,1); % 1st moment of source term is zero
cputime(1) = cputime(1)+toc;
%% ========================================================================
% Compute weighting matrix W for integration for 1D
% use simple midpoint quadrature rule
%========================================================================
W = par.dx*ones(1,par.n+1);
W (1) = 0.5 * par.dx; %boundary
W(end) = 0.5*par.dx; %boundary
inProd = diag(W);
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
%% ========================================================================
% Construct RHS metrix = [Theta(1)*S(1)+Theta(2)*S(2)+...]*Q
%========================================================================
S = zeros(par.n+1,par.n+1);
Sd = zeros(par.n+1,par.n+1);
for i = 1:5
    S = S + multS{i}*theta(i);
    Sd = Sd + multS{i}*theta(i);
end
%% ========================================================================
% Construct Sigma metrics = [Theta(1)*Sigma(1)+Theta(2)*Sigma(2)+...]*Psi
%========================================================================
sT{1} = zeros(par.n+1,par.n+1);
sT{2} = zeros(par.n+1,par.n+1);
 for i = 1:5
    sT{1} = sT{1}+multsT1{i}*theta(i);
    sT{2} = sT{2}+multsT2{i}*theta(i);
end
%% ========================================================================
% Computation of Moments
%========================================================================
 tic
 d = ones(par.n+1,1);
 a = par.alpha;
 M = sT{1} + Br - 1/3*D2*(sT{2}\D1);
 Mt = sT{1}'+Br' - 1/3*D1'*(sT{2}'\D2');
%% ========================================================================
% Constructing matrix
%========================================================================
Dose = par.dose(par.x,par)';
Op = [{M},{0.*diag(d)},{-S};
    {-a.*S*diag(d)},{Mt},{0.*diag(d)}
    {0.*diag(d)},{S'},{S*diag(d)}];
Rhs = [{zeros(par.n+1,1)}; {-a.*Sd*Dose}; {zeros(par.n+1,1)}];
Op = cell2mat(Op);
Rhs =cell2mat(Rhs);
sol=Op\Rhs;
psi{1} = sol(1:par.n+1,:);
psi{2} = sT{2}\(- 1/3*D1*psi{1});

lam{1} = sol(par.n+2:2*par.n+2,:);
lam{2} = sT{2}'\(D2'*lam{1});
q0 = sol(2*par.n+3:3*par.n+3,:);
cputime(2) = cputime(2)+toc; % time spent for solution
%% ========================================================================
% Output and Errors
%========================================================================
tic
if nargout(par.output) 
     output = struct('x',par.x);
     output.Q = q0;
     output.Lambda = lam0;
     output.U = psi0;
     output.cputime = cputime;
     par = par.output(par,par.x,output);
else
    output = struct('x',par.x);
    output.Q = q0;
    output.Lambda = lam(par.mom_output);
    output.U = psi(par.mom_output);
    output.cputime = cputime;
    par.output(par,par.x,output);
end
cputime(4) = cputime(4) + toc; %time spent for output/plot
if nargout,
     output = struct('x',par.x);
     output.Q = q0;
     output.Lambda = lam(par.mom_output);
     output.U = psi(par.mom_output);
     output.cputime = cputime;
     if par.flag.evaLB_KKT  % evaluate lower bound of KKT operator
        X = gen_xnorm(par.n+1,par.dx);
        Y = [X zeros(size(X)) zeros(size(X)); 
            zeros(size(X)) X zeros(size(X));
            zeros(size(X)) zeros(size(X)) X];
        A = Op'*(Y\Op);
        output.LB = sqrt(min(abs(eig(A,Y)))); % Lower bound of KKT operator
     end
     if par.flag.evaLB
         X = gen_xnorm(par.n+1,par.dx);
         A = M'*(X\M);
         output.LB = sqrt(min(abs(eig(A,X))));
     end
     if par.flag.evaUB
         X = gen_xnorm(par.n+1,par.dx);
         A = S'*(X\S);
         output.UB = sqrt(max(abs(eig(A,X))));
     end
     if par.flag.evaLUB
         X = gen_xnorm(par.n+1,par.dx);
         Yp = S;
         output.LUB = sqrt(max(abs(eig(Yp,X))));
     end
     if par.flag.evaRes % evaluate residual for full optimzation triple
         X = gen_xnorm(par.n+1,par.dx);
         Res = Op*par.opt_RB - Rhs;
         res_psi = Res(1:par.n+1);
         res_adjoint = Res(par.n+2:2*par.n+2);
         res_q = Res(2*par.n+3:end);
         nres_psi = (res_psi'*X^(-1)*res_psi);
         nres_q= (res_q'*X^(-1)*res_q);
         nres_adjoint = (res_adjoint'*X^(-1)*res_adjoint); 
         Res = sqrt(nres_psi+nres_q+nres_adjoint);
%        rel = sqrt(sol'*Y*sol);
         output.nRes = Res;%/rel; % Full residual
         output.nres_psi = nres_psi; % Norm of state residual
         output.nres_q = nres_q; % Norm of control residual
         output.nres_adjoint = nres_adjoint;% Norm of adjoint residual 
     end
     % lower bound
%      if ~isfield(par.RB,'LB')
%         LB  = output.LB;
%      else
%          LB = par.RB.LB;
%      end
%      % upper bound
%      if ~isfield(par.RB,'UB')
%         LB  = output.UB;
%      else
%          LB = par.RB.UB;
%      end
     if par.flag.alt_control % evaluate alt error for control
         Res = Op*par.opt_RB - Rhs; % full residual
         LB = par.RB.LB;
         UB = par.RB.UB;
         LUB = par.RB.LUB;
         % norm of each residual
         res_psi = Res(1:par.n+1);
         res_adjoint = Res(par.n+2:2*par.n+2);
         res_q = Res(2*par.n+3:end);
         nres_psi = sqrt(res_psi'*(X\res_psi));
         nres_q= sqrt(res_q'*(X\res_q));
         nres_adjoint = sqrt(res_adjoint'*(X\res_adjoint)); 
         % solve simple linear quadratic equation
         a = 1;
         b = -(nres_q+UB*nres_adjoint/LB);
         c = -(2*nres_psi*nres_adjoint/LB+par.alpha*LUB*nres_psi^2/(4*LB^2));
         e_q = (-b+sqrt(b^2-4*a*c))/(2*a); % control error bound
         output.alt_control = e_q;
     end
     if par.flag.alt_all_EB % evaluate alt.error for state,adj.,cost and control
         X = gen_xnorm(par.n+1,par.dx);
         Res = Op*par.opt_RB - Rhs; % full residual
         LB = par.RB.LB;
         UB = par.RB.UB;
         LUB = par.RB.LUB;
         % norm of each residual
         res_psi = Res(1:par.n+1);
         res_adjoint = Res(par.n+2:2*par.n+2);
         res_q = Res(2*par.n+3:end);
         nres_psi = sqrt(res_psi'*(X\res_psi));
         nres_q= sqrt(res_q'*(X\res_q));
         nres_adjoint = sqrt(res_adjoint'*(X\res_adjoint)); 
         % solve simple limear quadratic equation
         a = 1;
         b = -(nres_q+UB*nres_adjoint/LB);
         c = -(2*nres_psi*nres_adjoint/LB+par.alpha*LUB*nres_psi^2/(4*LB^2));
         e_q = (-b+sqrt(b^2-4*a*c))/(2*a); % control error bound
         e_psi = (nres_psi+UB*e_q)/LB; % state error bound
         e_adjoint = (nres_adjoint+par.alpha*LUB^2*e_psi)/LB; % adjoint EB
         % Output
         output.dose_error = e_psi; % state error bound
         output.adjoint_error = e_adjoint; % adjoint EB
         output.full_alt_error = sqrt(e_q^2+e_psi^2+e_adjoint^2); % full EB
         output.cost_alt_error = (nres_q*e_q+nres_adjoint*e_psi ... 
             +nres_psi*e_adjoint)/2; % Cost functional EB
         output.control_error = e_q; % Control EB
     end
end
end
%% ========================================================================
% Technical Functions
%========================================================================
function f = zero(varargin)
% Zero function.
f = zeros(size(varargin{1}));
end

function default_output(par,x,solution)
% Default plotting routine.
if par.flag.ifplot % plot, only if it is necessary
    figure;
    clf,
    subplot(4,1,1);
    plot(x,solution.U{1}),
    xlabel('x');
    ylabel('Optimal part.dist.');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on; 
    subplot(4,1,2);
    plot(x,par.dose(x,par)),
    xlabel('x');
    ylabel('Desired part.dist.');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    subplot(4,1,3);
    plot(x,solution.Lambda{1}),
    xlabel('x');
    ylabel('Adjoint solution');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    subplot(4,1,4);
    plot(x,solution.Q),
    xlabel('x');
    ylabel('Optimal source');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    if par.flag.save_plot
        backpath = pwd;
        cd ../Plots
        print('-dpsc',par.name)
        cd(backpath)
    end
end
end

function default_dose(par)
%default dose
    par.dose = ones(par.n+1,1);
end

function [par] = init_flag(par)
par.flag = struct(...
    'ifplot',0, ... % if it is not necessary to print the solution 
    'evaLB',0, ... % evaluate lower bound of the bilinear operator
    'evaRes',0, ... % evaluate residual: RBsolution - RHS
    'save_plot',0, ... % save the plot 
    'ifdisc_s',0,... % if source is discrete
    'half_plot',0, ... % half plot for comparison with analytical solution
    'alt_control',0, ... % evaluate control error bound using alt.approach
    'alt_all_EB', 0 ... % evaluate all errors using alt.approach
);
end
