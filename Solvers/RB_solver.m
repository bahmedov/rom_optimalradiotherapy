function [output] = RB_solver(par)
% RB - Solver
%    Solves P1 system of equations for boltzmann equation in 1D. Angular
%  dependece is approximated using Fourier expansion with Legendre Polyno-
%  mials. Then it projects the solution to the Rediced Basis space. 
%  
%% ========================================================================
% Author(s)
%========================================================================
% Created on 13.01.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |
%% ========================================================================
% Description
%========================================================================
% Input: 
%     par   - structure with all parameters
% Output: 
%    output - solution with RB bais function
%
%% ========================================================================
% Parameter Defaults
%========================================================================
if ~isfield(par,'name'),     par.name = 'Output'; end
if ~isfield(par,'arr_p'),    par.arr_p = [0.2 0.1 0.7 0.15]; end
if ~isfield(par,'arr_bp'),   par.arr_bp = [0.25 0.1 0.65 0.15]; end
if ~isfield(par,'ic'),       par.ic = @zero; end
if ~isfield(par,'sigma_a'),  par.sigma_a = @zero; end
if ~isfield(par,'sigma_s0'), par.sigma_s0 = @zero; end
if ~isfield(par,'sigma_sm'), par.sigma_sm = @zero; end
if ~isfield(par,'source'),   par.source = @zero; end
if ~isfield(par,'ax'),       par.ax = [0 1]; end
if ~isfield(par,'n'),        par.n = [100]; end
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'plot_fsize'), par.plot_fsize = 19; end% plots' font size
if ~isfield(par,'mom_output'), par.mom_output = 1; end % Moments plotted.
if ~isfield(par,'output'),   par.output = @default_output; end
% flags
if ~isfield(par,'flag'),     par.flag = init_flag(par); end
if ~isfield(par.flag,'ifplot'),   par.flag.ifplot = 0; end
if ~isfield(par.flag,'half_plot'), par.flag.half_plot = 0; end % not half-plot
if ~isfield(par.flag,'evaRB_LB'),    par.flag.evaRB_LB = 0; end % evaluate Lower Bound
if ~isfield(par.flag,'evaRes'),   par.flag.evaRes = 0; end % evaiate Residual
if ~isfield(par.flag,'material'), par.flag.material = 0; end % material function/constant
%% ========================================================================
% Initialization
%========================================================================
tic
cputime = zeros(1,4);
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
Phi1 = par.RB.Phi1;
Phi2 = par.RB.Phi2;
Br = par.RB.Br; 
multS = par.RB.multS;
multsT1 = par.RB.multsT1;
multsT2 = par.RB.multsT2;
D1 = par.RB.D1;
D2 = par.RB.D2;
Q{1} = par.source(par.x,0); % only zero-th moment of source is active
Q{2} = par.source(par.x,1); % 1st moment of source term is zero
N = size(Phi1,2);
cputime(1) = cputime(1)+toc;
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
%% ========================================================================
% Construct RHS metrix = [Theta(1)*S(1)+Theta(2)*S(2)+...]*Q
%========================================================================
S = zeros(N,par.n+1);
for i = 1:5
    S = S + multS{i}*theta(i);
end
%% ========================================================================
% Construct Sigma metrics = [Theta(1)*Sigma(1)+Theta(2)*Sigma(2)+...]*Psi
%========================================================================
sT{1} = zeros(N,N);
sT{2} = zeros(N,N);
for i = 1:5
    sT{1} = sT{1}+multsT1{i}*theta(i);
    sT{2} = sT{2}+multsT2{i}*theta(i);
end
%% ========================================================================
% Computation of Moments
%========================================================================
 tic
 In = (sT{2})^(-1);
 M = sT{1} + Br - 1/3*D2*(sT{2}\D1);
 Rhs = S*Q{1}';
 psi{1} = M \ Rhs;
 psi{2} = sT{2}\(S*Q{2}' - 1/3*D1*psi{1});
%% ========================================================================
% Re-construct the solution
%========================================================================
psi{1} = Phi1*psi{1}; % expand the RB solution with RB basis functions
psi{2} = Phi2*psi{2};
cputime(2) = cputime(2)+toc;
%% ========================================================================
% Output
%========================================================================
tic
if nargout(par.output) 
    par = par.output(par,par.x,psi(par.mom_output));
else
    par.output(par,par.x,psi(par.mom_output));
end
cputime(4) = cputime(4) + toc;
if nargout,
     output = struct('x',par.x);
     output.U = psi(par.mom_output);
     output.cputime = cputime;
    if par.flag.evaRB_LB 
        X = gen_xnorm(par.n+1,par.dx);
        X = par.RB.Phi1'*X*par.RB.Phi2;
        A = M'*X^(-1)*M;
        output.RB_LB = sqrt(min(abs(eig(A,X))));
     end
end
end
%% ========================================================================
% Technical Functions
%========================================================================
function f = zero(varargin)
% Zero function.
f = zeros(size(varargin{1}));
end

function default_output(par,x,U)
% Default plotting routine.
if par.flag.ifplot % plot, only if it is necessary
    if par.flag.half_plot & size(par.mom_output,2)>1 % half plot for comparison with analytical solution
        figure;
        for i = 1:size(par.mom_output,2)
            subplot(size(par.mom_output,2),1,i);
            plot(x,U{i}),
            xlabel('x');
            title(sprintf('%d-th moment of the reduced solution',par.mom_output(i)-1));
            set(gca,'FontSize',par.plot_fsize)
            ax = gca;
            ax.Title.FontWeight = 'normal';
        end
    end    
    if par.flag.half_plot == 0 % plot moments in separate figures
        for i = 1:size(par.mom_output,2)
            figure; 
            plot(x,U{i}),
            xlabel('x');
            title(sprintf('%d-th moment of the reduced solution',par.mom_output(i)-1));
            set(gca,'FontSize',par.plot_fsize)
            ax = gca;
            ax.Title.FontWeight = 'normal';
        end
    end
end
end

function [par] = init_flag(par)
par.flag = struct(...
    'ifplot',0, ... % if it is not necessary to print the solution 
    'evaRB_LB',0, ... % evaluate lower bound of the bilinear operator
    'evaRes',0, ... % evaluate residual: RBsolution - RHS
    'save_plot',0, ... % save the plot 
    'ifdisc_s',0,... % if source is discrete
    'half_plot',0 ... % half plot for comparison with analytical solution
);
end