function [output] = solver_adjoint(par)
% Solver
%    Solves P1 system of adjoint equations for boltzmann equation in 1D. 
%  Angular dependece is approximated using Fourier expansion with Legendre 
%  Polynomials. Then it is solved using Finite Elements Method (FEM). 
%  
%% ========================================================================
% Author(s)
%========================================================================
% Created on 18.12.2014 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 18.12.14 |    B.Ahmedov            | Created
%          |                         |
%          |                         |
%% ========================================================================
% Description
%========================================================================
% The equation to be solved:
%
% -mu*d(psi) / dx + sigma_t*psi = 
%   sigma_s*Int (s(x,mu*mu')psi(mu'))d(mu') + q(x)    
%
%
%% ========================================================================
% Parameter Defaults
%========================================================================
if ~isfield(par,'name'),     par.name = 'Output'; end
if ~isfield(par,'arr_p'),   par.arr_p = [0.2 0.1 0.7 0.15]; end
if ~isfield(par,'arr_bp'),  par.arr_bp = [0.25 0.1 0.65 0.15]; end
if ~isfield(par,'ic'),       par.ic = @zero; end
if ~isfield(par,'sigma_a'),  par.sigma_a = @zero; end
if ~isfield(par,'sigma_s0'), par.sigma_s0 = @zero; end
if ~isfield(par,'sigma_sm'), par.sigma_sm = @zero; end
if ~isfield(par,'source'),   par.source = @zero; end
if ~isfield(par,'ax'),       par.ax = [0 1]; end
if ~isfield(par,'n'),        par.n = [100]; end
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'ifplot'),   par.ifplot = 1; end
if ~isfield(par,'mom_output'), par.mom_output = 1; end % Moments plotted.
if ~isfield(par,'output'),   par.output = @default_output; end

%% ========================================================================
% Initialization
%========================================================================
tic
cputime = zeros(1,3);
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
cd Functions % go to the folder with additional functions
Br = gen_br_matrix(par.n+1); % values for bourdary elements
D1 = gen_d1matrix(par.n+1); % matrix at transport operator
D2 = -gen_d2matrix(par.n+1); % 
D2(1,1)=D2(1,1)*(-1);
D2(end,end)=D2(end,end)*(-1);
% Adjoint
D1 = -D1;
D2 = -D2;
S = gen_smatrix(par); % matrix at scattering operator
sT{1} = gen_sigma_matrix(par,0); % scattering and absorbtion at 0th m.
sT{2} = gen_sigma_matrix(par,1); % scattering and absorbtion at 1st m.
cd .. % go back to the original folder
Q{1} = par.source(par.x,0); % only zero-th moment of source is active
Q{2} = par.source(par.x,1); % 1st moment of source term is zero
% Q{1} = pi/10*cos(pi*par.x)+sin(pi*par.x);
% Q{2} = pi/3*cos(pi*par.x)+sin(pi*par.x)/10;
cputime(1) = cputime(1)+toc;
%% ========================================================================
% Check for errors
%========================================================================
% if det(sT{2}) == 0 
%     fprintf(2,'Unexpected termination: Scattering matrix is singular.\n');
%     return;
% end
%% ========================================================================
% Computation of Moments
%========================================================================
 tic
 In = (sT{2})^(-1);
 M = sT{1} + Br - 1/3*D2*In*D1;
 Rhs = S*Q{1}';
 psi{1} = M \ Rhs;
 psi{2} = In*(S*Q{2}' - 1/3*D1*psi{1});
 cputime(2) = cputime(2)+toc;
%% ========================================================================
% Construct the solution
%========================================================================
% cd Functions
% xp = par.x;
% M = length(par.x);
% for j = 1:length(xp)
% for i=1:M-1,
%   if xp(j) >=par.x(i) & xp(i) <= par.x(i+1)
%  y(j) = hat_function2(xp(j),par.x(i),par.x(i+1))*psi{par.mom_output}(i)...
%      + hat_function1(xp(j),par.x(i),par.x(i+1))*psi{par.mom_output}(i+1);
%    end
% end
% end
% psi{3} = y; 
% par.mom_output=3;
% cd ..
%% ========================================================================
% Output
%========================================================================
tic
if nargout(par.output) 
    par = par.output(par,par.x,psi{par.mom_output});
else
    par.output(par,par.x,psi(par.mom_output));
end
cputime(3) = cputime(3) + toc;
if nargout,
     output = struct('x',par.x);
     output.U = psi(par.mom_output);
end
fprintf('%s with P1 moments, %d points\n',... 
    par.name,par.n)   % Print test
cputime = reshape([cputime;cputime/sum(cputime)*1e2],1,[]); 
fprintf(['CPU-times\n Initialization:%7.2fs%5.0f%%\n',... % and CPU times.
    ' Solution:%13.2fs%5.0f%%\n Plotting:%13.2fs%5.0f%%\n'],cputime)
%% ========================================================================
% Technical Functions
%========================================================================
function f = zero(varargin)
% Zero function.
f = zeros(size(varargin{1}));

function default_output(par,x,U)
% Default plotting routine.
if par.ifplot % plot, only if it is necessary
    figure;
    clf,
    subplot(2,1,1);
    plot(x,U{1}),
    xlabel('x');
    ylabel('psi');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
end