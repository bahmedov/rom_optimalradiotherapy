function [output] = RB_optimal_solver(par)
% Solver
%    Solves optimality system minimization functional of deviation of 
%  an actual dose (solution of Boltzmann equation) from desired one. 
%  Angular dependece is approximated using Fourier expansion with Legendre Polyno-
%  mials. Then it is solved using Finite Elements Method (FEM). 
%  Optimality system is constructed as matrix and solved using Matlab
%  functions. 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 22.12.2014 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 30.04.15 | B.Ahmedov               | Loading RB functions/operators
%          |                         |instead of generating and projecting. 
%          |                         |
%% ========================================================================
% Description
%========================================================================
% Optimality system can be written as system of equations:
%
% |  M       0          0         0      -S  |  | Psi_0   |  | 0         |
% |1/3*D1  Sigma_t1     0         0       0  |  | Psi_1   |  | 0         |
% |a/2*S   3a/4*S       M         0       0  |* | Lambda_0|= |a/2*S*Psi_d| 
% | 0        0      -1/3*D1   Sigma_t1    0  |  | Lambda_1|  | 0         |   
% | 0        0        1/2        3/4      1  |  | Q       |  | 0         |
%
% For given desired particle distribution Psi_d it is necessary to find 
%   - Q  optimal source distribution
%
% Boltzmann Equation
% mu*d(psi) / dx + sigma_t*psi = 
%   sigma_s*Int (s(x,mu*mu')psi(mu'))d(mu') + q(x)    
%
%
%% ========================================================================
% Parameter Defaults
%========================================================================
if ~isfield(par,'name'),     par.name = 'Output'; end
if ~isfield(par,'arr_p'),    par.arr_p = [0.2 0.1 0.7 0.15]; end
if ~isfield(par,'arr_bp'),   par.arr_bp = [0.25 0.1 0.65 0.15]; end
if ~isfield(par,'ic'),       par.ic = @zero; end % default i.c.
if ~isfield(par,'sigma_a'),  par.sigma_a = @zero; end
if ~isfield(par,'sigma_s0'), par.sigma_s0 = @zero; end
if ~isfield(par,'sigma_sm'), par.sigma_sm = @zero; end
if ~isfield(par,'source'),   par.source = @zero; end % default source
if ~isfield(par,'ax'),       par.ax = [0 1]; end % size of domain
if ~isfield(par,'n'),        par.n = 100; end % number of disc. points
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'mom_output'),par.mom_output = 1; end % Moments plotted.
if ~isfield(par,'output'),   par.output = @default_output; end
if ~isfield(par,'dose'),     par.dose = @default_dose; end %default des.dose
if ~isfield(par,'alpha'),    par.alpha = 1; end % alpha in min.functional 
% flags
if ~isfield(par,'flag'),     par.flag = init_flag(par); end
if ~isfield(par.flag,'ifplot'),   par.flag.ifplot = 0; end
if ~isfield(par.flag,'half_plot'), par.flag.half_plot = 0; end % not half-plot
if ~isfield(par.flag,'evaLB'),    par.flag.evaLB = 0; end % evaluate Lower Bound
if ~isfield(par.flag,'evaRes'),   par.flag.evaRes = 0; end % evaiate Residual
if ~isfield(par.flag,'material'), par.flag.material = 0; end % material function/constant
%% ========================================================================
% Initialization
%========================================================================
tic
cputime = zeros(1,4);
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
Br = par.RB.Br; % values for bourdary elements
D1 = par.RB.D1; % matrix at transport operator
D2 = par.RB.D2; % matrix at adjoint operator
multS = par.RB.multS;
multSx = par.RB.multSx;
multSy = par.RB.multSy;
multSd = par.RB.multSd;
multsT1 = par.RB.multsT1;
multsT2 = par.RB.multsT2;
Dose = par.dose(par.x,par)';
Phi1 = par.RB.Phi1; % RB basis functions for 0-th moment of state
Phi2 = par.RB.Phi2; % RB basis functions for 1-st moment of state
N1 = size(Phi1,2);
N2 = size(Phi2,2);
S = zeros(N1,N2);
Sx = zeros(N1,N1);
Sy = zeros(N2,N2);
Sd = zeros(N1,par.n+1);
sT{1} = zeros(N1,N1);
sT{2} = zeros(N1,N1);
cputime(1) = cputime(1)+toc; % time spent for initialization
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
%% ========================================================================
% Construct RHS metrix = [Theta(1)*S(1)+Theta(2)*S(2)+...]*Q
%========================================================================
for i = 1:5
    S = S + multS{i}*theta(i);
    Sx = Sx +multSx{i}*theta(i);
    Sy = Sy +multSy{i}*theta(i);
    Sd = Sd +multSd{i}*theta(i);
end
%% ========================================================================
% Construct Sigma metrics = [Theta(1)*Sigma(1)+Theta(2)*Sigma(2)+...]*Psi
%========================================================================
for i = 1:5
    sT{1} = sT{1}+multsT1{i}*theta(i);
    sT{2} = sT{2}+multsT2{i}*theta(i);
end
%% ========================================================================
% Computation of Moments
%========================================================================
 tic
 a = par.alpha;
 M = sT{1} + Br - 1/3*D2*(sT{2}\D1);
 Mt = sT{1}'+Br' - 1/3*D1'*(sT{2}'\D2');
 cputime(2) = cputime(2)+toc; % part of the solution
%========================================================================
% Constructing matrix
%========================================================================
tic
Op = [{M},{zeros(N1,N1)},{-S};
    {a*Sx},{-Mt},{zeros(N1,N2)}
    {zeros(N2,N1)},{S'},{Sy}];
Rhs = [{zeros(N1,1)}; {a*Sd*Dose}; {zeros(N2,1)}];
sol=cell2mat(Op)\cell2mat(Rhs);
psi0 = sol(1:N1,:);
lam0 = sol(N1+1:2*N1,:);
q0 = sol(2*N1+1:end,:);
cputime(2) = cputime(2)+toc; %time spent for solution
%% ========================================================================
% Re-construct the solution
%========================================================================
tic 
psi0 = Phi1*psi0; % expand the RB solution with RB basis functions
lam0 = Phi1*lam0;
q0 = Phi2*q0;
cputime(3) = cputime(3) +toc; % time spent for reconstruction of full sol.
%% ========================================================================
% Output
%========================================================================
tic
if nargout(par.output) 
     output = struct('x',par.x);
     output.Q = q0;
     output.Lambda = lam0;
     output.U = psi0;
     output.cputime = cputime;
     par = par.output(par,par.x,output);
else
    output = struct('x',par.x);
    output.Q = q0;
    output.Lambda = lam0;
    output.U = psi0;
    output.cputime = cputime;
    par.output(par,par.x,output);
end
cputime(4) = cputime(4) + toc; % output/plotting time
if nargout,
     output = struct('x',par.x);
     output.Q = q0;
     output.Lambda = lam0;
     output.U = psi0;
     output.cputime = cputime;
%      if par.flag.evaLB
%          X = gen_xnorm(par.n+1,par.dx);
%          X = par.RB.Phi1'*X*par.RB.Phi1;
%          A = M'*X^(-1)*M;
%          output.LB = sqrt(min(abs(eig(A,X))));
%      end
%      if par.flag.evaUB
%          X = gen_xnorm(par.n+1,par.dx);
%          X = par.RB.Phi2'*X*par.RB.Phi2;
%          A = S'*X^(-1)*S;
%          output.UB = sqrt(max(abs(eig(A,X))));
%      end
end
end
%% ========================================================================
% Technical Functions
%========================================================================
function f = zero(varargin)
% Zero function.
f = zeros(size(varargin{1}));
end

function default_output(par,x,solution)
% Default plotting routine.
if par.flag.ifplot % plot, only if it is necessary
    figure;
    clf,
    subplot(4,1,1);
    plot(x,solution.U),
    xlabel('x');
    ylabel('Optimal part.dist.');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on; 
    subplot(4,1,2);
    plot(x,par.dose(x,par)),
    xlabel('x');
    ylabel('Desired part.dist.');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    subplot(4,1,3);
    plot(x,solution.Lambda),
    xlabel('x');
    ylabel('Adjoint solution');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    subplot(4,1,4);
    plot(x,solution.Q),
    xlabel('x');
    ylabel('Optimal source');
    title(sprintf('%s of moment order - %d',par.name,par.mom_output-1)),
    drawnow
    hold on;
    if par.flag.save_plot
        backpath = pwd;
        cd ../Plots
        print('-dpsc',par.name)
        cd(backpath);
    end
end
end

function default_dose(par)
%default dose
    par.dose = ones(par.n+1,1);
end

function [par] = init_flag(par)
par.flag = struct(...
    'ifplot',0, ... % if it is not necessary to print the solution 
    'evaLB',0, ... % evaluate lower bound of the bilinear operator
    'evaRes',0, ... % evaluate residual: RBsolution - RHS
    'save_plot',0, ... % save the plot 
    'ifdisc_s',0,... % if source is discrete
    'half_plot',0 ... % half plot for comparison with analytical solution
);
end