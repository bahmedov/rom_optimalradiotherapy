function ex_Forw_constMat_UpperBound
% - Generates RB space with various dimension N (e.g.N = 1,2,3,...). 
%
% - Runs reduced solver and finds upper bound (UB) of the bilinear
% operator b(*,*,p)
%
% - Finds maximum (UB) over tested parameters p 
%========================================================================
% Input: 
% -  Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)
% -  Material parameters are given as piecewise constant function 
%========================================================================
% Output: 
% - Plot UB over various parameters p
%% ========================================================================
% Author(s)
%========================================================================
% Created on 18.05.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 150,... % number of points in spatial domain
'ax', [0 1], ...
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
backpath = pwd;
cputime = zeros(1,4); % cputime
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
% initizalize the flags
par.flag.evaUB = 1;
par.flag.evaLB = 0;
par.flag.evaRes = 0;
% initialize progress bars
bar=progress_bar();
% random test parameter values
N1 = 100; N2 = 100;
tsnaps = 100; % total number of snapshots
rand_p{1} = par.p_l(1) + (par.p_u(1)-par.p_l(1)).*rand(N1,1);
rand_p{2} = par.p_l(2) + (par.p_u(2)-par.p_l(2)).*rand(N2,1);
rand_p{3} = par.p_l(3) + (par.p_u(3)-par.p_l(3)).*rand(N1,1);
rand_p{4} = par.p_l(4) + (par.p_u(4)-par.p_l(4)).*rand(N2,1);
%% ========================================================================
% Run RB solver vor various parameters in RB space with dimension k
%========================================================================
jp = 1;
for j = 1:size(rand_p{1},1)
    progress_bar(j,tsnaps,bar,'Computing UB for various parameters');
    par.arr_bp(1) = rand_p{1}(j);
    par.arr_bp(2) = rand_p{2}(j);
    par.arr_bp(3) = rand_p{3}(j);
    par.arr_bp(4) = rand_p{4}(j);
    gpar = par;
    % compute RB solution
    if pCheck(par.arr_bp(1),par.arr_bp(2),par.arr_bp(3),par.arr_bp(4))
        sol = solver(par);
        % compute the lower bound of a() in the RB spaces
        UB(jp) = sol.UB; 
        jp = jp+1;
    end
end
maxUB = max(UB);
delete(bar);
%% ========================================================================
% Print the results
%========================================================================
cprintf('*Magenta','Number of tested parameters: %d \n',jp);
cprintf('*Magenta','Maximum UB is: %2.3f \n',maxUB);
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);