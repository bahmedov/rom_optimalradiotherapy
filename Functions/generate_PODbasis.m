function [Phi] = generate_PODbasis(S,tol,N)
% Generates set of orthonormal basises using POD decomposition:
% Input: 
%       S - set of snapshots
%     tol - svd tolerence, i.d. only singular values begger than tol.
%           will be included to the basis functions set
% Output:
%     Phi - one of the orthonormal vectors (left or right) in 
%           SVD decomposition until the precision tol.
%% ========================================================================
% Author(s)
%========================================================================
% Created on 13.02.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 13.02.15 |    B.Ahmedov            | Create the routine
%          |                         |
%% ========================================================================
% Initialization
%========================================================================
 Phi = []; % set of basis functions
%% ========================================================================
% Construction of Basis Functions
%========================================================================
[U,S,V] = svd(S);
if nargin == 2
    sigma = diag(S); % singular values in array
    for l = 2:size(sigma) 
        serror = sum(sigma(l:end)); 
        if (serror <=tol) % if sum of unused singular values are l.t. tol
            Phi = U(:,1:l);
            return;
        end
    end
else
    if nargin == 3
        Phi = U(:,1:N);
    end
end
