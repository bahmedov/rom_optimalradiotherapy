function [bar] =  progress_bar(cur,total,bar,text)
%  Shows progress bar
%  Input:
%    cur   -  current progress
%    total -  total number
%    h     -  initialized progress bar
%% ========================================================================
% Parameter Defaults
%========================================================================
if nargin 
    if nargin == 4
    text = [text sprintf(' %d%% ',floor(cur*100/total))];
        waitbar(cur/total,bar,text);
    else
        waitbar(cur/total,bar,sprintf('In progress: %d%% ',...
    floor(cur*100/total)))
    end    
else
    bar = waitbar(0,'Initializing progress bar...'); % initialize progress bar
end
wax = findobj(bar, 'type','axes');
tax = get(wax,'title');
set(tax,'fontsize',14)