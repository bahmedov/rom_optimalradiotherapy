function [X] = gen_xnorm(N,dx)
% Generate X-Matrix
%    Creates metrix X norm. 
%  Will be used in solver.m for computation of Lower Bound
%    
%   
%   
%% ========================================================================
% Description
%========================================================================
%          |   1/3   1/6    0     ...  ...   |
%          |   1/6   2/3   1/6    ...  ...   |
% Sm =  dx*|   ...   ...   ...    ...  ...   |
%          |   ...   ...    0     1/6  1/3   | 
%
%% ========================================================================
% Initialization
%========================================================================
d = zeros(N,1); % init diag.elements
ud = zeros(N-1,1);
%% ========================================================================
% Computation of the Matrix
%========================================================================
d(1) = 1/3;
d(end)= 1/3;
d(2:end-1) = 2/3;
ud(:)=  1/6;
ld = ud;
X = dx*diag(ld,-1) + dx*diag(d) + dx*diag(ud,1); % construct X norm