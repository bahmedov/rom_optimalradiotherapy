function [Sm] = gen_multSd_matrices(par)
% Generate S-Matrix
%    Creates multiple metrices Sd for spatial integration of Dose
%  Will be used in solver.m
%    
%   Date of creation: 27.05.2015 
%   Author: Bahodir Ahmedov
%% ========================================================================
% Description
%========================================================================
%          |   1/2     1/2     0     ...       ...     |
%          |   1/2     1      1/2    ...       ...     |
% S1 =  dx*|   ...     1/2     1    ...       ...     |
%          |   ...     ...     0      0         0      | 
%          |    0      0     0    ...    ...   |
%          |    0      0     0    ...    ...   |
% S2 =  dx*|   ...    ...   1/2    1      0    |
%          |   ...    ...   1/2    1     1/2   | 
%...

%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
%% ========================================================================
% The elements at each section
%========================================================================
% multiply elements to 1e4 in order to make better comparison
% there is a problem in matlab with float number comparison
% e.g. 0.1+0.1+0.1 != 0.3 
par.ax = uint32(1e4*par.ax);
par.arr_p = uint32(1e4*par.arr_p);
x = uint32(1e4*x);
% define elements in each section
block{1} = ... % # of elements in 1st section
    (find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
block{2} = ... % # of elements in 2nd section
    (find(x>=(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
block{3} = ... % # of elements in 3rd section
    (find(x>=(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
block{4} = ... % # of elements in 4th section
    (find(x>=(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
block{5} = ... % # of elements in 5th section
    (find(x>=(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2)));
%% ========================================================================
% Computation of the Matrices
%========================================================================
for i = 1:5
   d = zeros(size(x,2),1); % init diag.elements
   ud = zeros(size(x,2)-1,1); % init.upper and lower diag.elements
   d(block{i}(1)) = 1/2*dx;
   d(block{i}(end)) = 1/2*dx;
   d(block{i}(2:end-1)) = dx;
   ud(block{i}(1:end-1)) = 1/2*dx;
   S = diag(ud,-1)+diag(ud,1)+diag(d);
   Sm{i} = S;
end
