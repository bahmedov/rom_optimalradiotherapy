function [Z_column,norm_contr,var_taken] = gen_basis_GSortho(Z,n_reiterations,Ghat)
%  Compute new basis vector from the Gram-Schmidt orthonormalization algorithm.
%% ========================================================================
% Description
%========================================================================
%   This function computes the new column of the orthonormalization.
%	If norm_contr is to small then it returns an empty column.
%	
%	Inputs:
%	Z: Basis matrix, last column is the new vector that has to be orthogonalized
%	N_reiterations: number of reiterations
%		(sometimes one iterations is not enough to obtain orthogonality)
%	Ghat: The inner product matrix that is used for the orthogonalization

%	
%	Outputs:
%	Z_column: the new orthogonalized basis vector
%	norm_contr: normalization factor in the Gram schmidt routine
%		(if this is small the new vector is almost contained in the span of the existing basis vectors,
%		hence an indicator for linear dependence)
%	var_taken: boolean variable that indicates whether the new vector was taken into the basis
%		(false if the new vector is numerically linear dependent to the other basis vectors)
%% ========================================================================
% Initialization
%========================================================================
 norm_contr_tol = 1e-12;	
 norm_contr = zeros(n_reiterations,1);
 var_taken = 1;
 N = size(Z,2);
%% ========================================================================
% Generate new basis
%========================================================================
 norm_var = sqrt(Z(:,N)'*Ghat*Z(:,N));
 if norm_var<1E-14
	norm_var = 1;
 end
 Z(:,N) = Z(:,N)/norm_var;
 for i = 1:n_reiterations
	for k = 1:N-1
		Z(:,N) = Z(:,N) - ( Z(:,k)'*Ghat*Z(:,N) ) * Z(:,k);
	end
	norm_contr(i) = sqrt( Z(:,N)'*Ghat*Z(:,N) );
	if (norm_contr(i) > norm_contr_tol)
		Z(:,N) = Z(:,N) / norm_contr(i);
 	else
 		var_taken = 0;
 		fprintf(1,'Z was discarded with norm_contr = %g \n',norm_contr(i));
 		%norm_contr
 		Z_column = [];
 		break;
 	end
	Z_column = Z(:,N);
 end
end
