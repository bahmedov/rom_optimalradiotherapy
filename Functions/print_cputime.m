function print_cputime(par,cputime)
%  Prints CPU time to the editor from parameters ('par') struct.
%% ========================================================================
% Parameter Defaults
%========================================================================
if ~isfield(par,'name'),     par.name = 'P1-FEM'; end
%% ========================================================================
% Print the CPU time
%========================================================================
fprintf('%s with P1 moments, %d points\n',... 
    par.name,par.n)   % Print test
tot_time = sum(cputime);
cputime = reshape([cputime;cputime/sum(cputime)*1e2],1,[]); % and CPU times.
cprintf('*Blue', 'CPU-times: \n');
fprintf(['Initialization:%20.2fs%5.0f%%\n',... 
  ' Solution:%26.2fs%5.0f%%\n Basis generation/Projection:%7.2fs%5.0f%%\n',...
  ' Plotting:%26.2fs%5.0f%%\n Total CPU Time: %19.2fs\n'],cputime,tot_time)
