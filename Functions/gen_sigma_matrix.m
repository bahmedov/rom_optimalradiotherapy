function [sTM] = gen_sigma_matrix(par,l)
% Generate BR-Matrix
%   Creates matrix Sigma for scattering and absorving coefficients.
%  Will be used in solver.m
% Generate aTheta-Matrix
%   Creates metrix aTheta in order to divide the domain to 
% several part, where the scattering parameters are different.
% Generate S-Matrix
%    Creates metrix S for spatial discretization of source term Q.
%  Will be used in solver.m
%% ========================================================================
% Description
%========================================================================
%          | 1/3*D_0   1/6*UD_0    0         ...       ...     |
%          | 1/6*LD_1  1/3*D_1  1/6*UD_1     ...       ...     |
% sTM = dx*|   ...      ...       ...        ...       ...     |
%          |   ...      ...        0      1/6*LD_N  1/3*D_N    | 
%   
% D_0 = 3/5*sT(1)*aTheta(1)+3/20*(sT(1)*aTheta(2)+sT(2)*aTheta(1))+1/10*sT(2)*aTheta(2)
% D_N = 3/5*sT(N)*aTheta(N)+3/20*(sT(N)*aTheta(N-1)+sT(N-1)*aTheta(N))+1/10*sT(N-1)*aTheta(N-1)
% D_j =
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+1/10*sT(j+1)*aTheta(j+1)+...
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+1/10*sT(j-1)*aTheta(j-1)
% UD_j = 3/10*sT(j)*aTheta(j)+1/10*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+3/10*sT(j+1)*aTheta(j+1), j = 0,...,N-1
% LD_j = 3/10*sT(j-1)*aTheta(j-1)+1/10*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+3/10*sT(j)*aTheta(j), j = 1,...,N
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
block_size = zeros(5,1); % # of elements in each section
d = zeros(size(x,2),1); % init diag.elements
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
% numer of elements at each section
block_size(1) = ... % # of elements in 1st section
    length(find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
block_size(2) = ... % # of elements in 2nd section
    length(find(x>(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
block_size(3) = ... % # of elements in 3rd section
    length(find(x>(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
block_size(4) = ... % # of elements in 4th section
    length(find(x>(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
block_size(5) = ... % # of elements in 5th section
    length(find(x>(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2)));
% construct aTheta - Matrix
aTheta = ones(1,block_size(1))*theta(1);
for i = 2:length(block_size)
    aTheta = [aTheta,  ones(1,block_size(i))*theta(i)];
end  
%% ========================================================================
% Computation of the sigma vector
%========================================================================
sA = par.sigma_a(x);
sS = par.sigma_s0(x);
sS = sS - par.sigma_sm(x,l);
sT = sA + sS;
%% ========================================================================
% Computation of final matrix
%========================================================================
ud =3/10*sT(1:end-1).*aTheta(1:end-1)+1/5*(sT(1:end-1).*aTheta(2:end)+...
    sT(2:end).*aTheta(1:end-1))+3/10*sT(2:end).*aTheta(2:end);
ld = ud;
d(1) = 3/5*sT(1).*aTheta(1)+3/20*(sT(1).*aTheta(2)+sT(2).*aTheta(1))+...
    1/10*sT(2).*aTheta(2);
d(end) = 3/5*sT(end).*aTheta(end)+3/20*(sT(end).*aTheta(end-1)+...
    sT(end-1).*aTheta(end))+1/10*sT(end-1).*aTheta(end-1);
d(2:end-1) = 3/5*sT(2:end-1).*aTheta(2:end-1)+3/20*(sT(2:end-1).*...
aTheta(3:end)+sT(3:end).*aTheta(2:end-1))+1/10*sT(3:end).*aTheta(3:end)+...
3/5*sT(2:end-1).*aTheta(2:end-1)+3/20*(sT(2:end-1).*aTheta(1:end-2)+...
sT(1:end-2).*aTheta(2:end-1))+1/10*sT(1:end-2).*aTheta(1:end-2);   

sTM = 1/6*dx*diag(ud,1)+1/3*dx*diag(d)+1/6*dx*diag(ld,-1);