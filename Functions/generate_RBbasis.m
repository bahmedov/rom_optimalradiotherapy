function [Phi] = generate_RBbasis(S,inProd)
% Generates set of orthonormal basises in RB space. 
%  For a given set of snapshots, generates set of orthonormal basis funcs.
% using Gramm - Schmidt algorithm. Take snapshots randomly. 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 13.01.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 13.01.15 |    B.Ahmedov            | Greate the routine
%          |                         |
%          |                         |
%% ========================================================================
% Description
%========================================================================
% Input: 
%     S    -  set of samples or snapshots for different parameters
%   inProd -  inner product matrix with respect to which the basis
%             functions should be orthonormal
% Output: 
%    Phi   -  set of orthonormalized basis functions in RB space
%
%% ========================================================================
% Initialization
%========================================================================
 N = size(S,2); % number of snapshots 
 Phi = []; % set of basis functions
%% ========================================================================
% Construction of Basis Functions
%========================================================================
 for i = 1:N
    B_temp = gen_basis_GSortho([Phi S(:,i)],3,inProd);
    if ~isempty(B_temp)
        Phi = [Phi B_temp];
    end
end