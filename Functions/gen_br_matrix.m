function [BR] = gen_br_matrix(N)
% Generate BR-Matrix
%   Creates metrix B for boundary elements.
%  Will be used in solver.m
%   
%   
%% ========================================================================
% Description
%========================================================================
%     |  1  0  ...  ... |
%     |  0  0  ...  ... |
% B = |  ...   ...  ... |,  R = Int_{-1}^{1} |mu| 1/2 P_0 P_0 d(mu) * B 
%     |  ...   ...   1  |
%
%  Since P_0(mu) = 1, then Int_{-1}^{1} |mu| 1/2 P_0 P_0 d(mu) = 1 
%% ========================================================================
% Initialization
%========================================================================
d = zeros(N,1); % diagonal
%% ========================================================================
% Computation of the Matrices
%========================================================================
d(1) = 1; d(end) = 1;
B = diag(d); % consruct R - Matrix
% for P1 when there is only one even component(P_0), R = B
BR = B;
