function [Phi] = generate_vPODbasis(S,tol,N)
% Generates set of orthonormal basises using POD decomposition:
% Input: 
%       S - set of snapshots
%     tol - svd tolerence, i.d. only singular values begger than tol.
%           will be included to the basis functions set
% Output:
%     Phi - one of the orthonormal vectors (left or right) in 
%           SVD decomposition until the precision tol.
%% ========================================================================
% Author(s)
%========================================================================
% Created on 13.02.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 13.02.15 |    B.Ahmedov            | Create the routine
%          |                         |
%% ========================================================================
% Initialization
%========================================================================
 Phi = []; % set of basis functions
%% ========================================================================
% Construction of Basis Functions
%========================================================================
[U,S,V] = svd(S);
if nargin == 2
    Phi = V(:,diag(S)>=tol);
else
    if nargin == 3
        Phi = V(:,1:N);
    end
end
