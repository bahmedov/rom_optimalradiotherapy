function [b] = pCheck(p1,p2,p3,p4)
% 
%   Checks if the randomly generated parameters are admissible
% Input: 
%    
% p1,p2,p3,p4 - original geometry parameters
%
%           
% Output:
% 
%      b: 1-  yes
%         0-  no
%  
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 25.05.15 |    B.Ahmedov            | Create the routine
%          |                         |
%% ========================================================================
% Initialization
%========================================================================
b = 1;
%% ========================================================================
% Parameter check
%========================================================================
% check if the intervals do not intersect
if (p1-p2 < p1+p2) && (p1+p2 < p3-p4) && (p3-p4<p3+p4)
    b = 1;
else 
    b = 0;
end
% check if intervals are in [0;1]
if (p1-p2<=0) || (p4+p3>=1)
    b = 0 ;
end
