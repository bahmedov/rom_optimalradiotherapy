function [sTM] = gen_multi_sigma_matrices(par,l)
% Generate BR-Matrix
%   Creates miltiple matrices of Sigma for scattering. absorb. coefficients.
%  Will be used in solver.m
% Generate aTheta-Matrix
%   Creates metrix aTheta in order to divide the domain to 
% several part, where the scattering parameters are different.
% Generate S-Matrix
%    Creates metrix S for spatial discretization of source term Q.
%  Will be used in solver.m
%
%   Date of creation: 08.02.2015 
%   Author: Bahodir Ahmedov
%% ========================================================================
% Description
%========================================================================
%          | 1/3*D_0   1/6*UD_0    0         ...       ...     |
%          | 1/6*LD_1  1/3*D_1  1/6*UD_1     ...       ...     |
% sTM = dx*|   ...      ...       ...        ...       ...     |
%          |   ...      ...        0      1/6*LD_N  1/3*D_N    | 
%   
% D_0 = 3/5*sT(1)*aTheta(1)+3/20*(sT(1)*aTheta(2)+sT(2)*aTheta(1))+1/10*sT(2)*aTheta(2)
% D_N = 3/5*sT(N)*aTheta(N)+3/20*(sT(N)*aTheta(N-1)+sT(N-1)*aTheta(N))+1/10*sT(N-1)*aTheta(N-1)
% D_j =
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+1/10*sT(j+1)*aTheta(j+1)+...
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+1/10*sT(j-1)*aTheta(j-1)
% UD_j = 3/10*sT(j)*aTheta(j)+1/10*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+3/10*sT(j+1)*aTheta(j+1), j = 0,...,N-1
% LD_j = 3/10*sT(j-1)*aTheta(j-1)+1/10*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+3/10*sT(j)*aTheta(j), j = 1,...,N
%% ========================================================================
% Initialization
%========================================================================
if ~isfield(par.flag,'material'), par.flag.material = 0; end % material function/constant
dx = par.dx;
x = par.x;
%% ========================================================================
% Computation of the sigma vector
%========================================================================
sA = par.sigma_a(x);
sS = par.sigma_s0(x);
sS = sS - par.sigma_sm(x,l);
sT = sA + sS;
if par.flag.material 
%% ========================================================================
% The elements at each section
%========================================================================
    % multiply elements to 1e4 in order to make better comparison
    % there is a problem in matlab with float number comparison
    % e.g. 0.1+0.1+0.1 != 0.3 
    par.ax = uint32(1e4*par.ax);
    par.arr_p = uint32(1e4*par.arr_p);
    x = uint32(1e4*x);
    % define elements in each section
    block{1} = ... % # of elements in 1st section
        (find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
    block{2} = ... % # of elements in 2nd section
        (find(x>=(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
    block{3} = ... % # of elements in 3rd section
        (find(x>=(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
    block{4} = ... % # of elements in 4th section
        (find(x>=(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
    block{5} = ... % # of elements in 5th section
        (find(x>=(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2))); 

%========================================================================
% Computation of final matrix
%========================================================================
    for i = 1:5
       d = zeros(size(x,2),1); % init diag.elements
       d(block{i}(1)) = 1/2;
       d(block{i}(end)) = 1/2;
       d(block{i}(2:end-1)) = 1;
       S = sT(i)*dx*diag(d); 
       sTM{i} = S;
    end
else    
    %% ========================================================================
    % The elements at each section
    %========================================================================
    % multiply elements to 1e4 in order to make better comparison
    % there is a problem in matlab with float number comparison
    % e.g. 0.1+0.1+0.1 != 0.3 
    par.ax = uint32(1e4*par.ax);
    par.arr_p = uint32(1e4*par.arr_p);
    x = uint32(1e4*x);
    % define elements in each section
    block{1} = ... % # of elements in 1st section
        (find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
    block{2} = ... % # of elements in 2nd section
        (find(x>=(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
    block{3} = ... % # of elements in 3rd section
        (find(x>=(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
    block{4} = ... % # of elements in 4th section
        (find(x>=(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
    block{5} = ... % # of elements in 5th section
        (find(x>=(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2))); 

%========================================================================
% Computation of final matrix
%========================================================================
    for i = 1:5
       d = zeros(size(x,2),1); % init diag.elements
       ud = zeros(size(x,2)-1,1); % init.upper and lower diag.elements
       d(block{i}(1)) = 3/4*sT(block{i}(1))+1/4*sT(block{i}(2));
       d(block{i}(end)) = 3/4*sT(block{i}(end))+1/4*sT(block{i}(end-1));
       d(block{i}(2:end-1)) = 1/4*sT(block{i}((1:end-2))) ...
           + 3/2*sT(block{i}((2:end-1)))+1/4*sT(block{i}((3:end)));
       ud(block{i}(1:end-1)) = 1/2*sT(block{i}((1:end-1)))...
           +1/2*sT(block{i}((2:end)));
       S = 1/6*dx*diag(ud,-1) + 1/3*dx*diag(d) + 1/6*dx*diag(ud,1); 
       sTM{i} = S;
    end
end