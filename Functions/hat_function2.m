function output = hat_function2(x,x1,x2)
% Hat function 2 
%  Returns value of Hat functions at the point x between nodes x1 and x2. 
% Will be used in solver.m to construct the solution
%   
%   
%% ========================================================================
% Description
%========================================================================
%             x2 - x  
%   output =  --------
%             x2 - x1   
%% ========================================================================
% Hat function 2
%========================================================================

    output = (x2-x)/(x2-x1);

  return
