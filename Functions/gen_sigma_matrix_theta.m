function [sTM] = gen_sigma_matrix_theta(par,l)
% Generate BR-Matrix
%   Creates matrix Sigma for scattering and absorving coefficients.
%  Will be used in solver.m
% Generate aTheta-Matrix
%   Creates metrix aTheta in order to divide the domain to 
% several part, where the scattering parameters are different.
% Generate S-Matrix
%    Creates metrix S for spatial discretization of source term Q.
%  Will be used in solver.m
%% ========================================================================
% Description
%========================================================================
%          | 1/3*D_0   1/6*UD_0    0         ...       ...     |
%          | 1/6*LD_1  1/3*D_1  1/6*UD_1     ...       ...     |
% sTM = dx*|   ...      ...       ...        ...       ...     |
%          |   ...      ...        0      1/6*LD_N  1/3*D_N    | 
%   
% D_0 = 3/5*sT(1)*aTheta(1)+3/20*(sT(1)*aTheta(2)+sT(2)*aTheta(1))+1/10*sT(2)*aTheta(2)
% D_N = 3/5*sT(N)*aTheta(N)+3/20*(sT(N)*aTheta(N-1)+sT(N-1)*aTheta(N))+1/10*sT(N-1)*aTheta(N-1)
% D_j =
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+1/10*sT(j+1)*aTheta(j+1)+...
% 3/5*sT(j)*aTheta(j)+3/20*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+1/10*sT(j-1)*aTheta(j-1)
% UD_j = 3/10*sT(j)*aTheta(j)+1/10*(sT(j)*aTheta(j+1)+sT(j+1)*aTheta(j))+3/10*sT(j+1)*aTheta(j+1), j = 0,...,N-1
% LD_j = 3/10*sT(j-1)*aTheta(j-1)+1/10*(sT(j)*aTheta(j-1)+sT(j-1)*aTheta(j))+3/10*sT(j)*aTheta(j), j = 1,...,N
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
d = zeros(size(x,2),1); % init diag.elements
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
aTheta=x+1; % it is to test a solver for different thetas (non-constant) 
%% ========================================================================
% Computation of the sigma vector
%========================================================================
sA = par.sigma_a(x);
sS = par.sigma_s0(x);
sS = sS - par.sigma_sm(x,l);
sT = sA + sS;
%% ========================================================================
% Computation of final matrix
%========================================================================
ud =3/10*sT(1:end-1).*aTheta(1:end-1)+1/5*(sT(1:end-1).*aTheta(2:end)+...
    sT(2:end).*aTheta(1:end-1))+3/10*sT(2:end).*aTheta(2:end);
ld = ud;
d(1) = 3/5*sT(1).*aTheta(1)+3/20*(sT(1).*aTheta(2)+sT(2).*aTheta(1))+...
    1/10*sT(2).*aTheta(2);
d(end) = 3/5*sT(end).*aTheta(end)+3/20*(sT(end).*aTheta(end-1)+...
    sT(end-1).*aTheta(end))+1/10*sT(end-1).*aTheta(end-1);
d(2:end-1) = 3/5*sT(2:end-1).*aTheta(2:end-1)+3/20*(sT(2:end-1).*...
aTheta(3:end)+sT(3:end).*aTheta(2:end-1))+1/10*sT(3:end).*aTheta(3:end)+...
3/5*sT(2:end-1).*aTheta(2:end-1)+3/20*(sT(2:end-1).*aTheta(1:end-2)+...
sT(1:end-2).*aTheta(2:end-1))+1/10*sT(1:end-2).*aTheta(1:end-2);   

sTM = 1/6*dx*diag(ud,1)+1/3*dx*diag(d)+1/6*dx*diag(ld,-1);