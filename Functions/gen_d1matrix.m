function [D1m] = gen_d1matrix(N)
% Generate D1-Matrix
%   Creates metrix D1 for spatial discretization of odd moments.
%  Will be used in solver.m
%   
%   
%% ========================================================================
% Description
%========================================================================
%       | -1/2  1/2  0   ...  ...   |
%       | -1/2   0  1/2  ...  ...   |
% D_1 = |       ...  ...  ...       |
%       |  ...  ...   0   -1/2  1/2 | 
%% ========================================================================
% Initialization
%========================================================================
d = zeros(N,1); % diagonal
ud = ones(N-1,1); % upper diagonal (in this case lower diagonal also)
%% ========================================================================
% Computation of the Matrix
%========================================================================
d(1) = 1/2; d(end) = -1/2;
D = diag(d); % consruct diagonal elements
ud = ud*1/2;
U = diag(ud,1); % construc upper diagonal elements
L = diag(-ud,-1); % construct lower diagonal elements
D1m = L + D + U; % construct D1 - Matrix

% d = ones(N,1); d(end)= 0; 
% ud = -1*ones(N-1,1);
% D1m = diag(ud,-1)+diag(d);
