function [Sm] = gen_smatrix_test(par)
% Generate S-Matrix
%    Creates metrix S for spatial discretization of source term Q.
%  Will be used in solver.m
%    
%   
%   
%% ========================================================================
% Description
%========================================================================
%          | 1/3*D_0   1/6*UD_0    0         ...       ...     |
%          | 1/6*LD_1  1/3*D_1  1/6*UD_1     ...       ...     |
% Sm =  dx*|   ...      ...       ...        ...       ...     |
%          |   ...      ...        0      1/6*LD_N  1/3*D_N    | 
%
% D_0 = 3/4*lTheta(0)+1/4*lTheta(1);
% D_N = 3/4*lTheta(N)+1/4*lTheta(N-1);
% D_j = 1/4*lTheta(j-1) + 3/2*lTheta(j)+1/4*lTheta(j+1);
% UD_j = 1/2*lTheta(j)+1/2*lTheta(j+1);
% LD_j = 1/2*lTheta(j)+1/2*lTheta(j-1);
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
d = zeros(size(x,2),1); % init diag.elements
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
aTheta = x+1; % it is to test a solver for different thetas (non-constant) 
%% ========================================================================
% Computation of the Matrix
%========================================================================
d(1) = 3/4*aTheta(1)+1/4*aTheta(2);
d(end)= 3/4*aTheta(end)+1/4*aTheta(end-1);
d(2:end-1) = 1/4*aTheta(1:end-2) + 3/2*aTheta(2:end-1)+1/4*aTheta(3:end);
ud =  1/2*aTheta(1:end-1)+1/2*aTheta(2:end);
ld = ud;
Sm = 1/6*dx*diag(ld,-1) + 1/3*dx*diag(d) + 1/6*dx*diag(ud,1); % construct S - Matrix