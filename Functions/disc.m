function [y] = disc(par)
% Evaluates original discretization points
% Input: 
%     par.x - refence discretization points
% par.arr_p - reference geometry parameters
% par.arr_bp- original geometry parameters
%
%           
% Output:
% 
%       y - original geometry discretization
%  
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 25.05.15 |    B.Ahmedov            | Create the routine
%          |                         |
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
block_size = zeros(5,1); % # of elements in each section
%% ========================================================================
% Initialization
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
% numer of elements at each section
block_size(1) = ... % # of elements in 1st section
    length(find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
block_size(2) = ... % # of elements in 2nd section
    length(find(x>(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
block_size(3) = ... % # of elements in 3rd section
    length(find(x>(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
block_size(4) = ... % # of elements in 4th section
    length(find(x>(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
block_size(5) = ... % # of elements in 5th section
    length(find(x>(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2)));
% construct aTheta - Matrix
adx = ones(1,block_size(1))*theta(1);
for i = 2:length(block_size)
    adx = [adx,  ones(1,block_size(i))*theta(i)];
end 
adx = adx.*dx;
y(1) = 0;
for i = 1:par.n-1
    y(i+1) = y(i)+adx(i);
end
y(par.n+1) = 1;
