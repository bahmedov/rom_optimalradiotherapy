function [M] = gen_metrics(par,ind)
% Generate - Metrics for linear form for the parameter with index "ind"
%   Creates sparse matrix for the lienar form with the parameter "ind"
%    
%   
%   
%% ========================================================================
% Description
%========================================================================
%          | 1/3   1/6    0      ...       ...     |
%          | 1/6   1/3   1/6     ...       ...     |
%  M =  dx*| 0     1/6   1/3     ...       ...     |
%          | ...     ...         ...       ...     |
%
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
%% ========================================================================
% Computation of indeces
%========================================================================
% element indices at each group/section
block{1} = ... % # of elements in 1st section
    find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2)));
block{2} = ... % # of elements in 2nd section
    find(x>=(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2)));
block{3} = ... % # of elements in 3rd section
    find(x>=(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4)));
block{4} = ... % # of elements in 4th section
    find(x>=(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4)));
block{5} = ... % # of elements in 5th section
    find(x>=(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2));
%% ========================================================================
% Computation of the Matrix
%========================================================================
d = zeros(par.n+1,1);
ud = zeros(par.n,1);
switch ind 
    case 1
        d(block{ind}) = 2/3*dx;
        if block{ind}(end) == block{ind+1}(1)
            d(block{ind}(end)) = 1/3*dx;
        else
            block{ind} = [block{ind} block{ind}+1];
            d(block{ind}(end)) = 1/3*dx;
        end
        d(block{ind}(1)) = 1/3*dx;
         
    case 2
        d(block{ind}) = 2/3*dx;
        if (block{ind}(end) == block{ind+1}(1))
            d(block{ind}(end)) = 1/3*dx;
        else
            block{ind} = [block{ind} block{ind}+1];
            d(block{ind}(end)) = 1/3*dx;
        end
        d(block{ind}(1)) = 1/3*dx;
    case 3
        d(block{ind}) = 2/3*dx;
        if (block{ind}(end) == block{ind+1}(1))
            d(block{ind}(end)) = 1/3*dx;
        else
            block{ind} = [block{ind} block{ind}+1];
            d(block{ind}(end)) = 1/3*dx;
        end
        d(block{ind}(1)) = 1/3*dx;
    case 4
        d(block{ind}) = 2/3*dx;
        if (block{ind}(end) == block{ind+1}(1))
            d(block{ind}(end)) = 1/3*dx;
        else
            block{ind} = [block{ind} block{ind}+1];
            d(block{ind}(end)) = 1/3*dx;
        end
        d(block{ind}(1)) = 1/3*dx;
    case 5
        d(block{ind}) = 2/3*dx;
        d(block{ind}(end)) = 1/3*dx;
        d(block{ind}(1)) = 1/3*dx;
end
ud(block{ind}(1:end-1)) = 1/6*dx;
ld = ud;
M = diag(ld,-1) + diag(d) + diag(ud,1); % construct M - Matrix