function [cost_fun] = cost_fun(psi,q,par)
% Evaluates cost functional 
% Input: 
%     psi - particle distribution
%      q  - control
%           
% Output:
%     Cost functional = alpha/2 int_0^1 (psi_0 - psi_d)^2+1/d int_0^1(q)^2   
%  
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
% 26.02.15 |    B.Ahmedov            | Create the routine
%          |                         |
%% ========================================================================
% Initialization
%========================================================================
cost_fun = 0;
%% ========================================================================
% Compute weighting matrix W for integration for 1D
% use simple midpoint quadrature rule
%========================================================================
W = par.dx*ones(1,par.n+1);
W (1) = 0.5 * par.dx; %boundary
W(end) = 0.5*par.dx; %boundary
%% ========================================================================
% Construction of Basis Functions
%========================================================================
cost_fun=W*(par.alpha.*(psi-par.dose(par.x,par)').^2+q.^2)/2;