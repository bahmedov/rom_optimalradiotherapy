function [par] =  RBload(path,par)
%  Shows progress bar
%  Input:
%    path  -  name of the data base
%    par   -  par structure which has all the parameters
% Output:
%    par   -  parameters with basis functions defined
%% ========================================================================
% Parameter Defaults
%========================================================================
 path = [pwd '/../RBbasis/' path '.mat'];
 load(path); % load the file
%% ========================================================================
% Load Basis Functions and Projected Operators
%========================================================================
% necessary variables
 RB.Phi1 = Phi1;
 RB.Phi2 = Phi2;
 RB.Br = Br; 
 RB.multS = multS;
 RB.multsT1 = sT1;
 RB.multsT2 = sT2;
 RB.D1 = D1;
 RB.D2 = D2;
% additional variables 
 if exist('multSx','var'),  RB.multSx = multSx;    end
 if exist('multSy','var'),  RB.multSy = multSy;    end
 if exist('multSd','var'),  RB.multSd = multSd;    end
 if exist('LB','var'),      RB.LB = LB;            end
 if exist('UB','var'),      RB.UB = UB;            end
 if exist('LUB','var'),     RB.LUB = LUB;          end
 if exist('offline_time','var'), RB.offline_time = offline_time; end
% output
 par.RB = RB; 