function [D2m] = gen_d2matrix(N)
% Generate D2-Matrix
%   Creates metrix D2 for spatial discretization of even moments.
%  Will be used in solver.m
%   
%   
%% ========================================================================
% Description
%========================================================================
%       | -1/2  -1/2  0   ...  ... |
%       |  1/2   0  -1/2  ...  ... |
% D_2 = |       ...  ...  ...      |
%       |  ...  ...   0   1/2  1/2 | 
%% ========================================================================
% Initialization
%========================================================================
d = zeros(N,1); % diagonal
ud = ones(N-1,1); % upper diagonal (in this case lower diagonal also)
%% ========================================================================
% Computation of the Matrix
%========================================================================
d(1) = -1/2; d(end) = 1/2;
D = diag(d); % consruct diagonal elements
ud = ud*1/2;
U = diag(-ud,1); % construc upper diagonal elements
L = diag(ud,-1); % construct lower diagonal elements
D2m = L + D + U; % construct D2 - Matrix