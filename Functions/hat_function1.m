function output = hat_function1(x,x1,x2)
% Hat function 1 
%  Returns value of Hat functions at the point x between nodes x1 and x2. 
% Will be used in solver.m to construct the solution
%   
%   
%% ========================================================================
% Description
%========================================================================
%              x - x1  
%   output =  --------
%             x2 - x1   
%% ========================================================================
% Hat function 1
%========================================================================

    output = (x-x1)/(x2-x1);

  return
