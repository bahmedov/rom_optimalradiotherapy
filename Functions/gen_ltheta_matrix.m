function [lTheta] = gen_ltheta_matrix(par)
% Generate lTheta-Matrix
%   Creates metrix aTheta in order to divide the domain to 
% several part, where the scattering parameters are different.
%  Will be used in solver.m
%   
%   
%% ========================================================================
% Description
%========================================================================
%           |  theta(1)   ...        ...    |
%           |     0     theta (1)    ...    |
% Theta_l = |    ...        ...      ...    |   
%           |     0         ...    theta(5) |     
%
%  theta(1) = (p1-p2)/(bp1-bp2);
%  theta(2) = (p2)/(bp2);
%  theta(3) = (p3-p4-p1-p2)/(bp3-bp4-bp1-bp2);
%  theta(4) = (p4)/(bp4);
%  theta(5) = (1-p3-p4)/(1-bp3-bp4);
%  theta(6) = 1;
%% ========================================================================
% Initialization
%========================================================================
x = par.x;
block_size = zeros(5,1); % # of elements in each section
%% ========================================================================
% Computation of the Matrices
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
% numer of elements at each section
block_size(1) = ... % # of elements in 1st section
    length(find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
block_size(2) = ... % # of elements in 2nd section
    length(find(x>(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
block_size(3) = ... % # of elements in 3rd section
    length(find(x>(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
block_size(4) = ... % # of elements in 4th section
    length(find(x>(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
block_size(5) = ... % # of elements in 5th section
    length(find(x>(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2)));

% construct aTheta - Matrix
d = ones(1,block_size(1))*theta(1);
for i = 2:length(block_size)
    d = [d,  ones(1,block_size(i))*theta(i)];
end
lTheta = diag(d);