function [Sm] = gen_smatrix(par)
% Generate S-Matrix
%    Creates metrix S for spatial discretization of source term Q.
%  Will be used in solver.m
%    
%   
%   
%% ========================================================================
% Description
%========================================================================
%          | 1/3*D_0   1/6*UD_0    0         ...       ...     |
%          | 1/6*LD_1  1/3*D_1  1/6*UD_1     ...       ...     |
% Sm =  dx*|   ...      ...       ...        ...       ...     |
%          |   ...      ...        0      1/6*LD_N  1/3*D_N    | 
%
% D_0 = 3/4*lTheta(0)+1/4*lTheta(1);
% D_N = 3/4*lTheta(N)+1/4*lTheta(N-1);
% D_j = 1/4*lTheta(j-1) + 3/2*lTheta(j)+1/4*lTheta(j+1);
% UD_j = 1/2*lTheta(j)+1/2*lTheta(j+1);
% LD_j = 1/2*lTheta(j)+1/2*lTheta(j-1);
%% ========================================================================
% Initialization
%========================================================================
dx = par.dx;
x = par.x;
block_size = zeros(5,1); % # of elements in each section
d = zeros(size(x,2),1); % init diag.elements
%% ========================================================================
% Computation of the aTheta vector
%========================================================================
% computation of thetas
theta(1) = (par.arr_p(1)-par.arr_p(2))/(par.arr_bp(1)-par.arr_bp(2));
theta(2) = (par.arr_p(2))/(par.arr_bp(2));
theta(3) = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) / ...
    (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
theta(4) = (par.arr_p(4))/(par.arr_bp(4));
theta(5) = (1-par.arr_p(3)-par.arr_p(4))/(1-par.arr_bp(3)-par.arr_bp(4));
theta = 1./theta;
% numer of elements at each section
block_size(1) = ... % # of elements in 1st section
    length(find(x>=par.ax(1) & x<=(par.arr_p(1)-par.arr_p(2))));
block_size(2) = ... % # of elements in 2nd section
    length(find(x>(par.arr_p(1)-par.arr_p(2)) & x<=(par.arr_p(1)+par.arr_p(2))));
block_size(3) = ... % # of elements in 3rd section
    length(find(x>(par.arr_p(1)+par.arr_p(2)) & x<=(par.arr_p(3)-par.arr_p(4))));
block_size(4) = ... % # of elements in 4th section
    length(find(x>(par.arr_p(3)-par.arr_p(4)) & x<=(par.arr_p(3)+par.arr_p(4))));
block_size(5) = ... % # of elements in 5th section
    length(find(x>(par.arr_p(3)+par.arr_p(4)) & x<=par.ax(2)));
% construct aTheta - Matrix
aTheta = ones(1,block_size(1))*theta(1);
for i = 2:length(block_size)
    aTheta = [aTheta,  ones(1,block_size(i))*theta(i)];
end  
%% ========================================================================
% Computation of the Matrix
%========================================================================
d(1) = 3/4*aTheta(1)+1/4*aTheta(2);
d(end)= 3/4*aTheta(end)+1/4*aTheta(end-1);
d(2:end-1) = 1/4*aTheta(1:end-2) + 3/2*aTheta(2:end-1)+1/4*aTheta(3:end);
ud =  1/2*aTheta(1:end-1)+1/2*aTheta(2:end);
ld = ud;
Sm = 1/6*dx*diag(ld,-1) + 1/3*dx*diag(d) + 1/6*dx*diag(ud,1); % construct S - Matrix