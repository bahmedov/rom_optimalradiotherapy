function ex_optim_pDose_constMat_mRegPar
%   Finds optimal control for an example with given parameters and
% desired dose distribution for various regularization parameter \alpha.
%========================================================================
% Output: 
% - Plots in one figure optimal control solutions vor various
% regularization parameter \alpha
% - Plots the error btw solution and desired dose as a function of
% regularization parameter \alpha (convergence rate)
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 02.06.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-optim-pDose-constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp', [0.2 0.07 0.8 0.07],... % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
des_dose = dose(par.x,par);
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Computation of the actual geometry metrix
%========================================================================
ref_x = par.x;     % the reference geometry discretization
par.x = disc(par); % the actual geometry discretization
%% ========================================================================
% Ful optimal solver
%========================================================================
 mAlpha = [1e0 1e1 1e2 1e3 1e4 1e5 1e6];% alphas to be tested
 quad = ones(1,size(par.x,2))*par.dx;
 quad(1) = 1/2*quad(1);
 quad(end) = 1/2*quad(1);
 des_dose = dose(par.x,par);
 for i = 1:size(mAlpha,2)
    par.alpha = mAlpha(i) ;
    solution = optimal_solver(par);             % Run optimal solver.
    q1(:,i) = solution.Q; % optimal control
    psi1(:,i) = solution.U{1}; % optimal state
    psi2 = solution.U{2};
    diff = psi1(:,i)'-des_dose;
    l2err(i) = sqrt(quad*(diff.*diff)'); % compute the error
    l2inf(i) = norm(psi1(:,i)'-des_dose,inf); % compute l-inf error
 end

%% ========================================================================
% Plot the solution
%========================================================================
font_size = 19;
%============================================================
% Optimal state for various parameters
figure;
plot(par.x,des_dose,'r','LineWidth',1.5); hold on;
plot(par.x,psi1(:,5),'g-','LineWidth',1.5); hold on;
plot(par.x,psi1(:,4),'b-','LineWidth',1.5); hold on;
plot(par.x,psi1(:,7),'k-','LineWidth',1.5); hold off;
grid on
legend('Desired Dose','with alpha = 10^4','with alpha = 10^5','with alpha = 10^6');
xlabel('x')
title('Optimal state for various $\alpha$','Interpreter','latex'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';
% matlab2tikz('state_mRegVar', 'height', '\figureheight', 'width', '\figurewidth'); 
print('-depsc','state_mRegVar');
%============================================================
% Convergence rate
figure;
loglog(mAlpha,l2err,'r*--','LineWidth',2.0); hold on;
loglog(mAlpha,l2inf,'b*--','LineWidth',2.0); hold off;
grid on
legend('l2-error','linf-error');
xlabel('$\alpha$','Interpreter','latex');
title('Convergence rate'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';
%matlab2tikz('convergence_rate.tikz', 'height', '\figureheight', 'width', '\figurewidth'); 
print('-depsc','convergence_rate');
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*2;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);
 
function f = dose(x,par)
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
