function ex_optim_pDose_constMat
%   Finds optimal control for an example with given parameters and
% desired dose distribution.
%========================================================================
% Output: 
% - Finds optimal control for parametrized desired dose
%
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 05.05.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-optim-pDose-constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp', [0.2 0.07 0.8 0.07],... % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
des_dose = dose(par.x,par);
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% FE optimal solver
%========================================================================
 tic
 solution = optimal_solver(par);             % Run optimal solver.
 q1 = solution.Q;
 psi1 = solution.U{1};
 psi2 = solution.U{2};
 adjoint1 = solution.Lambda{1};
 adjoint2 = solution.Lambda{2}; 
 forw = toc;
 desired = dose(par.x,par);
%% ========================================================================
% Computation of the actual geometry metrix
%========================================================================
ref_x = par.x;     % reference geometry discretization
par.x = disc(par); % the actual geometry discretization
%% ========================================================================
% Plot the solution
%========================================================================
% desired dose in the original geometry
font_size = 17;
figure; 
plot(par.x,des_dose,'k','LineWidth',1.4);
ylim([-0.01 0.21])
y1=get(gca,'ylim');
hold on; plot([par.arr_bp(1)-par.arr_bp(2) par.arr_bp(1)-par.arr_bp(2)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_bp(1)+par.arr_bp(2) par.arr_bp(1)+par.arr_bp(2)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_bp(3)-par.arr_bp(4) par.arr_bp(3)-par.arr_bp(4)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_bp(3)+par.arr_bp(4) par.arr_bp(3)+par.arr_bp(4)],y1,'b--','LineWidth',1);
xlabel('x')
set(gca,'FontSize',font_size)
set(gca,'XTick',[0:0.1:1])
ax = gca;
ax.Title.FontWeight = 'normal';
%============================================================
% desired dose in reference geometry
font_size = 17;
figure; 
plot(ref_x,des_dose,'k','LineWidth',1.4);
ylim([-0.01 0.21])
y1=get(gca,'ylim');
hold on; plot([par.arr_p(1)-par.arr_p(2) par.arr_p(1)-par.arr_p(2)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_p(1)+par.arr_p(2) par.arr_p(1)+par.arr_p(2)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_p(3)-par.arr_p(4) par.arr_p(3)-par.arr_p(4)],y1,'b--','LineWidth',1);
hold on; plot([par.arr_p(3)+par.arr_p(4) par.arr_p(3)+par.arr_p(4)],y1,'b--','LineWidth',1);
xlabel('x')
set(gca,'FontSize',font_size)
set(gca,'XTick',[0:0.1:1])
ax = gca;
ax.Title.FontWeight = 'normal';
%============================================================
% optimal state in original geometry
figure;
plot(par.x,psi1,'b','LineWidth',1.5);
ylim([0.0 0.05])
grid on
xlabel('x')
title('Optimal state'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';
set(gca,'XTick',[0:0.1:1])
%print('-depsc','state_rightb');
%============================================================
% Optimal state in original geometry
figure;
plot(par.x,q1,'r','LineWidth',1.5);
ylim([0.0 0.3])
grid on
xlabel('x');
title('Optimal control'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';
set(gca,'XTick',[0:0.1:1])
%print('-depsc','control_rightb');

%============================================================
% Optimal adjoint in original geometry
figure;
plot(par.x,adjoint1,'m','LineWidth',1.5);
grid on
xlabel('x');
title('Optimal adjoint '),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';
%print('-depsc','adjoint');
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*2;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
