Reduced Bases for 1D Optimal Radiative Transfer Problem
================================================
We study numerical methods for inverse problems arising in cancer therapy treatment under uncertainty. The interest is on efficient and reliable numerical methods that allow to determine the influence of possible unknown parameters on the treatment plan for cancer therapy. The Boltzmann transport equation is used to model the evolution of charged particles in tissue. A mixed variational framework is presented  and existence and uniqueness of a weak solution is established. The optimality system is approximated using a low - dimensional reduced basis formulation based on a  PN - FE discretization. We derive *a posteriori* bounds for the error in the reduced basis solution of the optimal control problem with respect to the solution of the PN - FE discretization. Numerical results in slab geometry are presented to confirm the validity of our approach. This is a Software which contains all example/test files.

# Table of content
- [1. Introduction](#markdown-header-1-introduction)
- [2. Structure of the code](#markdown-header-2-structure-of-the-code)
	- [Example files](#markdown-header-21-example-files)
	- [Solvers](#markdown-header-22-solvers)
	- [Functions](#markdown-header-23-functions)     
		- [Progress Bar](#markdown-header-231-progress-bar)      
		- [CpuTime](#markdown-header-233-cputime)

- [3. Example of solution of transport equation](#markdown-header-3-example-of-solution-of-transport-equation)
	- [FE-solution](#markdown-header-31-fe-solution)
	- [RB-solution](#markdown-header-32-rb-solution)
- [4. Example of solution of optimal control problem](#markdown-header-4-example-of-solution-of-optimal-control-problem)
	- [FE-solution](#markdown-header-41-fe-solution)
	- [RB-solution](#markdown-header-42-rb-solution)

- [References](#markdown-header-references)

# 1.  Introduction
 We study reduced order modelling for optimal radiotherapy treatment plan. Boltzmann equation is used to model the interaction between radiative particles with tissue. The transport equation is numerically solved using PN -FEM method. At first, we solve optimization problems: minimizing the deviation from desired dose distribution. Then we consider a parameterized geometry. In offline stage we solve a problem for sampled parameter values. The online phase then consists of solving the reduced problem for the actual set of parameters. Error estimators for reduced model are derived and tested. Numerical results are presented.

#2. Structure of the code
In order to solve a problem one should prescribe example file, which calls solution functions. For communication between the files and function we use Matlab struct *par*, which should be prescribed in an example file. In order to solve a forward or optimal control problem, the example file calls corresponding solvers *solvers solver(par)* or *optim_solver(par)*. The following figure shows the structure of the code's files. 

![struct.png](https://bitbucket.org/repo/oKexpG/images/2834682404-struct.png)

## 2.1. Example files
Any of the examples files start with description of the Matlab struct *par*. Example of par fields is
~~~~
%==================================================================
% Problem Parameters
%==================================================================
par = struct(...
'n', 200,... % number of points in spatial domain
'ax', [0 1], ... % domain size
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moment (or multiple moments)
'arr_p', [0.3 0.07 0.8 0.07],... % reference geometry parameters 
'arr_bp',[0.3 0.07 0.7 0.07]... % actual geometry parameters
);
~~~~
where @sigma_a, @sigma_s0, @sigma_sm are handle functions which should be defined in the same example file. An example of such a handle function is:
~~~~
function f = source (x,m)
% source function for given position x and moment m
f = 2*pi^2/3*cos(2*pi*x)+cos(pi*x).^2+2;
f = f*(l==0); % the source term for only zero-th moment
~~~~
A brief description of the functions, which should be defined, are given in the following table:

| Function name | Description                          |
| ------------- | ------------------------------                |
| `sigma_a(x)`      | Absorption coefficient.  |
| `sigma_s0(x)`   | Total scattering coefficient.   |
| `sigma_sm(x,m)`   |  Moments of scattering kernel  |
| `source (x,m)`   | Given source function  |
| `dose(par,x)`   | Desired dose function  |
| `default_output(par,x,U)`   | Default plotting routine. |

In order to control some routines in the solver files one should prescribe flags. Example of the structured flags is the following:
~~~~
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',1, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',1 ... % plot the figures in one figure
);
~~~~
where, for example, 'ifplot' is a flag which calls plot function in a solver if it is "1" and does not plot the output if it is "0". If a user wants to save plots, one should initialize the flag 'save_plot' with "1". 

## 2.2. Solvers 
There are 4 files which solves a given problem:  
	(1) *solver(par)* solves the transport equation using PN-FE method;    
	(2) *rb_solver(par)* solves the transport equation in RB space;   
	(3) *optimal_solver(par)* solves a given optimal control problem using PN-FE method;  
	(4) *rb_optimal_solver(par)* solves a given optimal control problem in RB space.  
 
If any of the files of the parameter *par* is not giving, a solver start filling out it with default values:

```
#!matlab

if ~isfield(par,'name'),     par.name = 'Output'; end
if ~isfield(par,'arr_p'),    par.arr_p = [0.2 0.07 0.8 0.07]; end
if ~isfield(par,'arr_bp'),   par.arr_bp = [0.2 0.07 0.8 0.07]; end
if ~isfield(par,'ic'),       par.ic = @zero; end
if ~isfield(par,'sigma_a'),  par.sigma_a = @zero; end
if ~isfield(par,'sigma_s0'), par.sigma_s0 = @zero; end
if ~isfield(par,'sigma_sm'), par.sigma_sm = @zero; end
if ~isfield(par,'source'),   par.source = @zero; end
if ~isfield(par,'ax'),       par.ax = [0 1]; end
if ~isfield(par,'n'),        par.n = [100]; end
```

## 2.3. Functions
### 2.3.1 Progress Bar
### 2.3.3 Cputime
Cputime is a vector with CPU time spent for each of the following operations:  
	- cputime(1) - time spent for Initialization of all metrices, variables and tools  
	- cputime(2) - time spent for solution  
	- cputime(3) - time spent for basis generation, projection to the subspaces  
	- cputime(4) - times spent for output and plotting  

#3. Example of solution of transport equation
In order to solve the transport equation one should prescribe the scattering functions, as well as the source term. The example file *ex_Forw_costMat.m* solves the equation with piecewise constant material coefficient and given source function. 
```
#!matlab

function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
``` 
## 3.1. FE-Solution
We are interesting in a solution when the geometry of the problem is changing. That is why, it is necessary to generate geometry independent metrics once, and use them for various geometry parameters.

```
#!matlab
% generating solution metrics
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
```
If the metrics are not generated in the example file, the solver will generate the default metrices.

The solution of the transport equation with the given functions using PN-FE method is shown in the following figure:

![forw](https://bitbucket.org/repo/oKexpG/images/3404328192-forw.png)

## 3.2. RB-Solution
Reduced basis (RB) solution contains offline and online computational process. In offline stage we generate RB basis functions: we take snapshots of solution for train sample of parameters. Then, we use POD method with a given tolerance to construct orthogonal basis functions. In order to generate the RB basis functions for this example, one should execute *ex_RBforw_constMat_generate_RBspace*. It generate RB basis functions and saves them in the folder *RBbasis* with the name given in parameter *par.name*.

Now, another example file can use these basis function in order to solve a problem. It loads the basis function from the folder *RBbasis*. Please, note that the variable *par.name* should match in both example files. The solution using the PN-RB method is shown in the following figure:

![rb_forw.png](https://bitbucket.org/repo/oKexpG/images/1477862331-rb_forw.png)

The figure can be generated by executing the file *ex_RBforw_constMat*.

#4. Example of solution of optimal control problem
The process of solving an optimal control problem is similar to the solution of transport equation. An example file should contain a function which describes desired dose. In this example we used the following function to describe desired dose:

```
#!matlab

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);
```

The profile of the desired dose is given in the following figure:

![des_dose.png](https://bitbucket.org/repo/oKexpG/images/2804983570-des_dose.png)

The regularization parameter is constant in a whole interval *par.alpha* = 10. 
## 4.1. FE-Solution
The solution of the optimal control problem with prescribed functions is shown in the following figure:

![optim.png](https://bitbucket.org/repo/oKexpG/images/1403720103-optim.png)

## 4.2. RB-Solution
RB solution for the optimal control is again contains offline/online state. In the offline stage, one should generate the RB basis functions. In this example, this can be done by executing the example file *ex_RBoptim_constMat_generate_RBspace.m*. 

Once the RB basis functions are generated, one can solve the problem using RB method. The solution of the optimization triple is given in the following figure:

![rb_optim.png](https://bitbucket.org/repo/oKexpG/images/1212589824-rb_optim.png)

The figure can be generated using the example file *ex_RBoptim_constMat*.
# References #

1. B. Ahmedov, M. Herty and M. Frank, [Optimization of Approximation Models for Solving Kinetic Equations](https://www.researchgate.net/publication/269937565_Optimization_of_Approximation_Models_for_Solving_Kinetic_Equations)
2. M. Herty, M. Frank and A.N. Sandjo, [Optimal treatment planning governed by kinetic equations](https://www.researchgate.net/publication/24169284_Optimal_treatment_planning_governed_by_kinetic_equations)