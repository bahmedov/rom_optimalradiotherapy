README for 1D Optimal Control Problem 
================================================
This is Matlab code for 1D simulation of Optimal Cancer Treatment. It aims to test Reduced Bases methods. 

#Start here!
One can download the working copy here: 

###  Reduced Bases for 1D Optimal Control Problem
 We study reduced order modelling for optimal radiotherapy treatment plan. Boltzmann equation is used to model the interaction between radiative particles with tissue. The transport equation is numerically solved using PN -FEM method. At first, we solve optimization problems: minimizing the deviation from desired dose distribution. Then we consider a parameterized geometry. In offline stage we solve a problem for sampled parameter values. The online phase then consists of solving the reduced problem for the actual set of parameters. Error estimators for reduced model are derived and tested. Numerical results are presented.

### Example file
There are global "solver" to perform forward equation solution, "adjoint_solver" to perform adjoint equation solution, and "optimal_solver" to perform optimisation problem. It is necessary to call required solver from an example file. All the existing example, test files are in the root folder. Necessary data, parameters are conveyed to the solvers using structure "par".  Example of par fields is
~~~~
%==================================================================
% Problem Parameters
%==================================================================
par = struct(...
'n', 100,... % number of points in spatial domain
'ax', [0 1], ... % domain size
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',1,... % output moment (or multiple moments)
'ifplot',1,... % plot the solution
'arr_p', [0.3 0.2 0.7 0.15],... % reference geometry parameters 
'arr_bp',[0.3 0.2 0.7 0.15]... % actual geometry parameters
);
~~~~
where @sigma_a, @sigma_s0, @sigma_sm and @source are handle functions which should be defined here. An example of such a handle function is:
~~~~
function f = source (x,m)
% source function for given position x and moment m
f = 2*pi^2/3*cos(2*pi*x)+cos(pi*x).^2+2;
f = f*(l==0); % the source term for only zero-th moment
~~~~
A brief description of the functions, which should be defined, are given in the following table:

| Function name | Description                          |
| ------------- | ------------------------------                |
| `sigma_a(x)`      | Absorption coefficient.  |
| `sigma_s0(x)`   | Total scattering coefficient.   |
| `sigma_sm(x,m)`   |  Moments of scattering kernel  |
| `source (x,m)`   | Given source function  |
| `dose(x)`   | Desired dose function  |
| `default_output(par,x,U)`   | Default plotting routine. |

After the description of the structure "par", solver can be executed 
~~~~
solution=solver(par);                                         % Run solver.
~~~~
All the functions, parameters and variables can be in reviewed in one of example files, such as "ex_sol_cos", "ex_sol_sin_homo", etc... .

## Forward solver
Forward solver computes particle distribution for a given source term distribution. It takes the structure with given parameters in the variable "par". Output can be a particular moment (s) or the accumulated particle distribution, i.e. all the moments summered multiplied by the corresponding Legendre polynomials.  

## Adjoint solver
Solves an adjoint equation for a given residual (deviation) of actual particles distribution from desired particle distribution. 

## Solver for Optimisation problem
Finds optimal source for which deviation from desired dose distribution is minimal.  
### Iterative Solver
Optimisation problem can be solved using different way and algorithms. A standard way of solving such a problem is to find a gradient of a cost functional (which should be minimal) using adjoint equation and  use the gradient in one of a gradient algorithms, such as Newton iteration, Quasi-newton iteration, etc. 

Such as solver is implemented in Matlab and can be executed from an example file. It finds particle distribution for a given initial source term using "forward solver", then it finds the adjoint variable using "adjoint solver". Finally using gradient, which is computed using initial source term and the adjoint variable, it calls one of the iterative algorithms. 

### Matrix Solver
Another way to solve the optimisation problem is to solve optimality system (a.k.a. KKT system, first order necessary conditions) explicitly. 
We are interested in solving the following optimization problem:
![Screenshot 2015-01-01 20.54.23.png](https://bitbucket.org/repo/pX5bRB/images/4253265785-Screenshot%202015-01-01%2020.54.23.png)
The optimality system can written in matrix form as
![Screenshot 2015-01-01 20.51.56.png](https://bitbucket.org/repo/pX5bRB/images/3721789432-Screenshot%202015-01-01%2020.51.56.png)
where the optimal source term can be written using the equation
![Screenshot 2015-01-01 20.54.38.png](https://bitbucket.org/repo/pX5bRB/images/3593595549-Screenshot%202015-01-01%2020.54.38.png)