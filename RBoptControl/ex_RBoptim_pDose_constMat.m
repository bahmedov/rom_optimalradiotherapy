function ex_RBoptim_pDose_constMat
%   Solves an example with given parameters in RB space by loading 
% generated basis functions. 
%========================================================================
% Output: 
% - In order to compare the solution, runs FE optimal solver and plots 
% both solutions
%
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 29.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.1 0.8 0.1],... % param. values for reference geometry
'arr_bp',[0.2 0.1 0.8 0.1],...  % param. values for actual geometry
'p_l',[0.15 0.05 0.75 0.05],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
des_dose = dose(par.x,par);
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Ful optimal solver
%========================================================================
 tic
 solution = optimal_solver(par);             % Run optimal solver.
 q1 = solution.Q;
 psi1 = solution.U{1};
 psi2 = solution.U{2};
 adjoint1 = solution.Lambda{1};
 adjoint2 = solution.Lambda{2}; 
 forw = toc;
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
  backpath = pwd;
  cd ../RBbasis
  par = RBload(par.name,par); % load basis functions and projected operators
  cprintf('*Magenta','RB Basis functions are found and loaded!\n');
  cd(backpath);
%% ========================================================================
% RB solution
%========================================================================
 cputime = zeros(1,4);
 rb_solution = RB_optimal_solver(par);    % Run optimal solver.
 rb_q1 = rb_solution.Q;
 rb_psi1 = rb_solution.U;
 rb_adjoint1 = rb_solution.Lambda;
 cputime = cputime + rb_solution.cputime;
 cputime(3) = par.RB.offline_time;
 print_cputime(par,cputime);
 cprintf('Black',' Full FE solution:  %2.2es \n',forw);
 cprintf('Black',' RB solution:  %13.2es \n',cputime(2));
%% ========================================================================
% Comparison btw full solution and RB solution
%========================================================================
font_size = 15;
figure; 
subplot(2,2,1);
plot(par.x,des_dose);
xlabel('x')
title('Desired dose'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,2);
plot(par.x,psi1);
xlabel('x')
title('Optimal State'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,3);
plot(par.x,adjoint1);
xlabel('x');
title('Optimal Adjoint'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,4);
plot(par.x,q1);
xlabel('x');
title('Optimal Control'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

figure; 
subplot(2,2,1);
plot(par.x,des_dose);
xlabel('x')
title('Desired dose'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,2);
plot(par.x,rb_psi1);
xlabel('x')
title('RB Optimal State'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,3);
plot(par.x,rb_adjoint1);
xlabel('x');
title('RB Optimal Adjoint'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

subplot(2,2,4);
plot(par.x,rb_q1);
xlabel('x');
title('RB Optimal Control'),
set(gca,'FontSize',font_size)
ax = gca;
ax.Title.FontWeight = 'normal';

%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);


function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
