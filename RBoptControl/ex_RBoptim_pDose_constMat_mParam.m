function ex_RBoptim_pDose_constMat_mParam
%    Runs reduced solver for multiple parameters. Each parameter is chosen
% from indicated range. Compares the solution with "truth" solution for
% each parameter and plots L2 difference btw RB and "truth" solution. 
%   
%   Load RB space, which is generated in advance. Note, that par.name
% should be the same as in generating .m file. 
%========================================================================
% Output:
% - Error and bound for optimal control for various parameters
% - Error and bound for forward state for various parameters
% - Error and bound for adjoint state for various parameters
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 30.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.1 0.8 0.1],... % param. values for reference geometry
'arr_bp',[0.2 0.1 0.8 0.1],...  % param. values for actual geometry
'p_l',[0.15 0.05 0.75 0.05],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
backpath = pwd;
cd ../RBbasis
par = RBload(par.name,par); % load basis functions and projected operators
cprintf('*Magenta','RB Basis functions are found and loaded!\n');
cd(backpath);
%% ========================================================================
% Initialization
%========================================================================
cputime = zeros(1,4);
X = gen_xnorm(par.n+1,par.dx);
j = 1;
bar = progress_bar; % initialize progress bar
N1 = 100; N2 = 100;
tsnaps = 100; % number of testing parameter
%% ========================================================================
% RB solution
%========================================================================
% set output flags
par.flag.evaLB = 1;
par.flag.evaUB = 1;
par.flag.evaLUB = 1;
par.flag.alt_all_EB = 1;
% construct set of test parameters
rand_p{1} = par.p_l(1) + (par.p_u(1)-par.p_l(1)).*rand(N1,1);
rand_p{2} = par.p_l(2) + (par.p_u(2)-par.p_l(2)).*rand(N2,1);
rand_p{3} = par.p_l(3) + (par.p_u(3)-par.p_l(3)).*rand(N1,1);
rand_p{4} = par.p_l(4) + (par.p_u(4)-par.p_l(4)).*rand(N2,1);
for i = 1:size(rand_p{1},1)
    progress_bar(j,tsnaps,bar,'Running RB solver for various parameters:');
    par.arr_bp(1) = rand_p{1}(i);
    par.arr_bp(2) = rand_p{2}(i);
    par.arr_bp(3) = rand_p{3}(i);
    par.arr_bp(4) = rand_p{4}(i);
    gpar = par;
    % compute RB solution
    rb_solution = RB_optimal_solver(par); % Run RB optimal solver.
    rb_q1(1:size(rb_solution.Q,1),j) = rb_solution.Q;% rb optimal control
    rb_forw1(1:size(rb_solution.U,1),j) = rb_solution.U; % rb forward state
    rb_adjoint1(1:size(rb_solution.Lambda,1),j) = rb_solution.Lambda; % rb adjoint state
    par.opt_RB = [rb_solution.U; rb_solution.Lambda; rb_solution.Q]; % RB optimality triple
    cputime = cputime + rb_solution.cputime; % RB comput.time
    % compute full solution
    solution = optimal_solver(par); % FE solution (in order to compute error)
    EB.costfun(j) = solution.cost_alt_error; % bound for cost fun
    EB.control(j) = solution.control_error; % bound for control
    EB.forw(j) = solution.dose_error; % bound for forward state
    EB.adjoint(j) = solution.adjoint_error; % bound for adjoint
    forw1(1:size(solution.U{1},1),j) = solution.U{1}; % FE forward state
    adjoint1(1:size(solution.Lambda{1},1),j) = solution.Lambda{1}; % FE adjoint state
    q1(1:size(solution.Q,1),j) = solution.Q; % FE control
    cost = cost_fun(forw1(:,j),q1(:,j),par); % cost fun. value
%========================================================================
% Computation of L2 error
%========================================================================
     e_psi = forw1(:,j)-rb_forw1(:,j); 
     e_adj = adjoint1(:,j)-rb_adjoint1(:,j);
     e_q = q1(:,j) - rb_q1(:,j);
     ne_psi = e_psi'*X*e_psi;
     ne_adj = e_adj'*X*e_adj;
     ne_q = e_q'*X*e_q;
     cost = cost_fun(forw1(:,j),q1(:,j),par);
     e_cost = cost_fun(forw1(:,j),q1(:,j),par) - ...
         cost_fun(rb_forw1(:,j),rb_q1(:,j),par);
     l2err.costfun(j) = abs(e_cost);
     l2err.control(j) = sqrt(ne_q);
     l2err.forw(j) = sqrt(ne_psi);
     l2err.adjoint(j) = sqrt(ne_adj);  
%========================================================================
% Computation of effectivities
%========================================================================
     eff.control(j) = EB.control(j)/l2err.control(j);
     eff.forw(j)= EB.forw(j)/l2err.forw(j);
     eff.adjoint(j) = EB.adjoint(j)/l2err.adjoint(j);
     eff.costfun(j) = EB.costfun(j)/l2err.costfun(j);
     j = j+1;
 end
 delete(bar); % delete progress bar
 cputime(3) = par.RB.offline_time;
 print_cputime(par,cputime); % print overall CPU time 
%% ========================================================================
% L2-different btw the "truth" and RB solution for each parameter 
%========================================================================
font_size = 21;
par.flag.ifplot = 1;
if par.flag.ifplot
%========================================================================
% Control Error
    figure; 
    semilogy(l2err.control,'--*m','LineWidth',1.4);
    hold on
    semilogy(EB.control,'-.*b','LineWidth',1.4);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of control');
    set(gca,'FontSize',font_size)
%========================================================================
% Forward State Error
    figure; 
    semilogy(l2err.forw,'--*m','LineWidth',1.4);
    hold on
    semilogy(EB.forw,'-.*b','LineWidth',1.4);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of forward');
    set(gca,'FontSize',font_size)
%========================================================================
% Adjoint State Error
    figure; 
    semilogy(l2err.adjoint,'--*m','LineWidth',1.4);
    hold on
    semilogy(EB.adjoint,'-.*b','LineWidth',1.4);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of adjointforward');
    set(gca,'FontSize',font_size)
%========================================================================
% Const functional Error
    figure; 
    semilogy(l2err.costfun,'--*m','LineWidth',1.4);
    hold on
    semilogy(EB.costfun,'-.*b','LineWidth',1.4);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of cost fun.');
    set(gca,'FontSize',font_size)
end
%% ========================================================================
% Plot for effectivity
%========================================================================
ifplot_effectivity = 0;
if ifplot_effectivity
%========================================================================
% Control Error
    figure; 
    semilogy(eff.control);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of control');
    set(gca,'FontSize',font_size)
%========================================================================
% Forward State Error
    figure; 
    semilogy(eff.psi);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of forward');
    set(gca,'FontSize',font_size)
%========================================================================
% Adjoint State Error
    figure; 
    semilogy(eff.adjoint);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of adjointforward');
    set(gca,'FontSize',font_size)
%========================================================================
% Const functional Error
    figure; 
    semilogy(eff.costfun);
    xlabel('Various parameters')
    ylabel('Relative L_2 error of cost fun.');
    set(gca,'FontSize',font_size)
end
%% ========================================================================
% Scatter plot effectivity vs l2 error
%========================================================================
ifscatplot = 1;
a = 40; % scatter circle size (area)
if ifscatplot
%========================================================================
% Control Error
    figure; 
    scatter(l2err.control,eff.control,60,'LineWidth',2);
    xlabel('Error')
    ylabel('Effectivity');
    title('Control');
    % plot in log scale
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    % set font size
    ax = gca;
    ax.Title.FontWeight = 'normal';
    set(gca,'FontSize',font_size)
    cd ../Plots/RBoptim
        print('-depsc','RBoptim-mParam_scat-control30');
    cd(backpath); % go back to the original folder
%========================================================================
% Forward State Error
    figure; 
    scatter(l2err.forw,eff.forw,60,'LineWidth',2);
    xlabel('Error')
    ylabel('Effectivity');
    title('State solution');
    % plot in log scale
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    % set font size
    ax = gca;
    ax.Title.FontWeight = 'normal';
    set(gca,'FontSize',font_size)
    cd ../Plots/RBoptim
        print('-depsc','RBoptim-mParam_scat-forw30');
    cd(backpath); % go back to the original folder
%========================================================================
% Adjoint State Error
    figure; 
    scatter(l2err.adjoint,eff.adjoint,60,'LineWidth',2);
    xlabel('Error')
    ylabel('Effectivity');
    title('Adjoint solution');
    % plot in log scale
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    % set font size
    ax = gca;
    ax.Title.FontWeight = 'normal';
    set(gca,'FontSize',font_size)
    cd ../Plots/RBoptim
        print('-depsc','RBoptim-mParam_scat-adjoint30');
    cd(backpath); % go back to the original folder
%========================================================================
% Const functional Error
    figure; 
    scatter(l2err.costfun,eff.costfun,60,'LineWidth',2);
    xlabel('Error')
    ylabel('Effectivity');
    title('Cost functional');
    % plot in log scale
    set(gca,'xscale','log')
    set(gca,'yscale','log')
    % set font size
    ax = gca;
    ax.Title.FontWeight = 'normal';
    set(gca,'FontSize',font_size)
    cd ../Plots/RBoptim
        print('-depsc','RBoptim-mParam_scat-costfun30');
    cd(backpath); % go back to the original folder
end
%========================================================================
% maximum error over parameters
cprintf('*Blue','Maximum control error over parameters is: %2.2e \n',max(l2err.control));
cprintf('*Blue','Maximum forw.state error over parameters is: %2.2e \n',max(l2err.forw));
cprintf('*Blue','Maximum adj.state error over parameters is: %2.2e \n',max(l2err.adjoint));
cprintf('*Blue','Maximum cost fun. error over parameters is: %2.2e \n',max(l2err.costfun));
cprintf('*Blue','Number of tested parameters %d\n',tsnaps);
cprintf('*Blue','Dimensions of RB spaces %d and %d\n',size(par.RB.Phi1,2),size(par.RB.Phi2,2));
cprintf('*Blue','Dimensions of FE spaces %d and %d\n',4*par.n,par.n);
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
sc = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) ... 
    / (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
a = (par.arr_p(1)+par.arr_p(2))+0.01;
b = (par.arr_p(3)-par.arr_p(4))-0.01;
f = sc*0.2*trap(x,[0.3 0.40 0.58 0.7]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);