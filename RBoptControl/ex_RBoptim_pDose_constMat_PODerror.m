function ex_RBoptim_pDose_constMat_PODerror
%   Generates correlation matrix of snapshots for test parameter set. 
% Plots POD error decay. 
%
% Output: 
% - Takes snapshots for the training set of parameters
% - Constructs correlation matrix and finds POD
% - Plots decay of POD error 
% - Saves the plots in .eps format in the folder ../Plots/RBoptim
%
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 16.09.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat', ... % name for the example
'n', 200,... % number of points in spatial domain
'ax', [0 1], ... % spatial domain
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',1, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
backpath = pwd;
global gpar
bar = progress_bar;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% taking snapshots for training parameters
%========================================================================
% evaluate LB and UB 
par.flag.evaLB = 1;
par.flag.evaUB = 1;
par.flag.evaLUB = 1;
% initialization
j=1;tic
offline_time = 0;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
             progress_bar(j,tsnaps,bar,'Taking snapshots: ');
             par.arr_bp(1) = i;
             par.arr_bp(2) = k;
             par.arr_bp(3) = l;
             par.arr_bp(4) = m;
             gpar = par;
             solution = optimal_solver(par);      % Run optimal solver.
             q1(1:size(solution.Q,1),j) = solution.Q;
             psi1(1:size(solution.U{1},1),j) = solution.U{1};
             psi2(1:size(solution.U{2},1),j) = solution.U{2};
             adjoint1(1:size(solution.Lambda{1},1),j) = solution.Lambda{1};
             adjoint2(1:size(solution.Lambda{2},1),j) = solution.Lambda{2}; 
             offline_time = offline_time+sum(solution.cputime);
             LB(j) = solution.LB;
             UB(j) = solution.UB;
             LUB(j) = solution.LUB;
             j = j+1;
            end
        end
    end
end
delete(bar)% close progress bar
gtime = toc;
% set maximum UB and minimum LB
minLB = min(LB);
maxUB = max(UB);
maxLUB = max(LUB);
%% ========================================================================
% Construct correlation matrix and find singular values of it
%========================================================================
 z1 = [psi1 psi2 adjoint1 adjoint2];% basis for psi and adjoints
 z2 = q1; % basis for source term
 [U,S1,V] = svd(z1); 
 [U,S2,V] = svd(z2);
 sigma1 = diag(S1); % singular values 
 sigma2 = diag(S2); % singular values
 % finds POD error for state/adjoint space (sum of unused singular values)
 for i = 1:size(sigma1,1)
     pod_error1(i) = sum(sigma1(i+1:end));
 end
 % finds POD error for control space (sum of unused singular values)
 for i = 1:size(sigma2,1)
     pod_error2(i) = sum(sigma2(i+1:end));
 end
%% ========================================================================
% Plot the singular values
%========================================================================
font_size = 15;
% singular values for source matrix
figure; 
semilogy(1:size(pod_error1,2),pod_error1,'-m','LineWidth',1.4);
%hold on;
%semilogy(1:size(pod_error2,2),pod_error2,'-b','LineWidth',1.4);
%xlim([0 55])
ylim([10^-16 10^2])
%xlabel('$\mathcal{M}$', 'Interpreter', 'latex');
xlabel('POD-basis dimension');
title('POD-error');
set(gca,'FontSize',font_size)
set(gca,'XGrid','on');
set(gca,'YGrid','on');
set(gca, 'yminorgrid', 'off') % only plot major grid lines
ax = gca;
ax.Title.FontWeight = 'normal';
%%save as tikz if needed
%matlab2tikz('control_err.tikz', 'height', '\figureheight', 'width', '\figurewidth'); 
if par.flag.save_plot 
    cd ../Plots/RBoptim
        print('-depsc','PODerror_state-adjoint');
    cd(backpath); % go back to the original folder
end 
% singular values for state+adjoint matrix
figure; 
semilogy(1:size(pod_error2,2),pod_error2,'-m','LineWidth',1.4);
%xlim([0 55])
ylim([10^-12 10^2])
%xlabel('$\mathcal{M}$', 'Interpreter', 'latex');
xlabel('POD-basis dimension');
title('POD-error');
set(gca,'FontSize',font_size)
set(gca,'XGrid','on');
set(gca,'YGrid','on');
set(gca, 'yminorgrid', 'off') % only plot major grid lines
ax = gca;
ax.Title.FontWeight = 'normal';
%%save as tikz if needed
%matlab2tikz('control_err.tikz', 'height', '\figureheight', 'width', '\figurewidth'); 
if par.flag.save_plot 
    cd ../Plots/RBoptim
        print('-depsc','PODerror_control');
    cd(backpath); % go back to the original folder
end 
 %% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
