function ex_RBoptim_pDose_constMat_SpeedUP
% -  Checks speed-up of the RB solver with compare to the full FE solution
% - Runs FE solution for a test sample and take average time as 
% a FE time to compare
% - Runs RB solution for a test sample and take average time as 
% a RB time to compare
%
% - Runs reduced optimal solver for multiple parameters. Each parameter 
% is chosen randomly from indicated range. 
%
% -   Finds average error over tested parameters for each N. Computes error
% estimator and plots is in the same figure with "truth" error.
%   
%========================================================================
% Output:
% - Convergence of Error and bound for optimal control for var. RB dim.size
% - Convergence of Error and bound for forward state for var. RB dim.size
% - Convergence of Error and bound for adjoint state for var. RB dim.size
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 30.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',1, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
backpath = pwd;
cputime = zeros(1,4); % cputime
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
bar = progress_bar; % initialize progress bar
fetime = 0; % fe time spent for solution of the the test parameters
rbtime = 0; % rb time spent for solution of the test parameters
%% ========================================================================
% Initialization
%========================================================================
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multSd = gen_multSd_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Taking snapshots
%========================================================================
% does not evaluate LB and UB 
par.flag.evaLB = 0;
par.flag.evaUB = 0;
par.flag.evaLUB = 0;
% initialization
j=1;tic
offline_time = 0;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
             progress_bar(j,tsnaps,bar,'Taking snapshots: ');
             par.arr_bp(1) = i;
             par.arr_bp(2) = k;
             par.arr_bp(3) = l;
             par.arr_bp(4) = m;
             gpar = par;
             solution = optimal_solver(par);      % Run optimal solver.
             q1(1:size(solution.Q,1),j) = solution.Q;
             psi1(1:size(solution.U{1},1),j) = solution.U{1};
             psi2(1:size(solution.U{2},1),j) = solution.U{2};
             adjoint1(1:size(solution.Lambda{1},1),j) = solution.Lambda{1};
             adjoint2(1:size(solution.Lambda{2},1),j) = solution.Lambda{2}; 
             offline_time = offline_time+sum(solution.cputime);
             j = j+1;
            end
        end
    end
end
delete(bar)% close progress bar
gtime = toc;
%% ========================================================================
% Generate Basis functions in RB space and save them
%========================================================================
 z1 = [psi1 psi2 adjoint1 adjoint2];% basis for psi and adjoints
 z2 = q1; % basis for source term
 Phi2 = generate_PODbasis(z2,1e-10);
 M = size(Phi2,2);
 Phi1 = generate_PODbasis(z1,1e-8,2*M); % generate b.f. for 0th moment state
 sc = size(Phi1,2)/size(Phi2,2);
 % generate full metrices
 multS = gen_multS_matrices(par);
 multsT1 = gen_multi_sigma_matrices(par,0);
 multsT2 = gen_multi_sigma_matrices(par,1);
 Br = gen_br_matrix(par.n+1); % values for bourdary elements
 D1 = gen_d1matrix(par.n+1); % matrix at transport operator
 D2 = -D1';
 for i = 1:size(multS,2)
     multS{i} = Phi1'*multS{i}*Phi2;
     sT1{i} = Phi1'*multsT1{i}*Phi1;
     sT2{i} = Phi1'*multsT2{i}*Phi1;
 end
 Br = Phi1'*Br*Phi1;
 D1 = Phi1'*D1*Phi1;
 D2 = Phi1'*D2*Phi1;
 % save reduced metrices
 cd ../RBbasis
 save([par.name '.mat'],'Phi1','Phi2','multS','sT1','sT2', ...
     'Br','D1','D2','offline_time'); % save basis functions
 cd(backpath); % go back to the folder with the example
 cprintf('*Magenta','RB Basis functions for "%s" are generated!\n',par.name);
 cprintf('*Blue','Computational time: %3.4fs\n',gtime);
%% ========================================================================
% Initialization
%========================================================================
% set flags
par.flag.evaLB = 0;
par.flag.evaUB = 0;
par.flag.evaLUB = 0;
par.flag.alt_all_EB = 0;
X = gen_xnorm(par.n+1,par.dx); % inner products metrics
% initialize progress bars
bar = progress_bar; % initialize progress bar
% test parameter values
N1 = 100; N2 = 100;
tsnaps = 100; % total number of snapshots
rand_p{1} = par.p_l(1) + (par.p_u(1)-par.p_l(1)).*rand(N1,1);
rand_p{2} = par.p_l(2) + (par.p_u(2)-par.p_l(2)).*rand(N2,1);
rand_p{3} = par.p_l(3) + (par.p_u(3)-par.p_l(3)).*rand(N1,1);
rand_p{4} = par.p_l(4) + (par.p_u(4)-par.p_l(4)).*rand(N2,1);
nd = 5; % number of tested dimension
j1 = 1; % 
%% ========================================================================
% Loop for various RB dimension
%========================================================================
k = 35; % loop until RB space with dim = 55
    clear RB S1 S1_rb S2_rb
    RB.Phi1 = Phi1(:,1:floor(k*sc));
    RB.Phi2 = Phi2(:,1:k);
    % generate full metrices
    multS = gen_multS_matrices(par);
    if par.flag.material % if mat.coef are piecewise constant
       multsT1 = gen_multi_sigma_matrixa(par,0);
       multsT2 = gen_multi_sigma_matrixa(par,1);
      else % if mat.coef are spatial dependent
       multsT1 = gen_multi_sigma_matrix(par,0);
       multsT2 = gen_multi_sigma_matrix(par,1);
    end
    Br = gen_br_matrix(par.n+1); % values for bourdary elements
    D1 = gen_d1matrix(par.n+1); % matrix at transport operator
    D2 = -D1';
    for i = 1:size(multS,2)
        RB.multSx{i} = RB.Phi1'*multS{i}*RB.Phi1;
        RB.multSy{i} = RB.Phi2'*multS{i}*RB.Phi2;
        RB.multSd{i} = RB.Phi1'*multS{i};
        RB.multS{i} = RB.Phi1'*multS{i}*RB.Phi2;
        RB.multsT1{i} = RB.Phi1'*multsT1{i}*RB.Phi1;
        RB.multsT2{i} = RB.Phi1'*multsT2{i}*RB.Phi1;
    end
    RB.Br = RB.Phi1'*Br*RB.Phi1;
    RB.D1 = RB.Phi1'*D1*RB.Phi1;
    RB.D2 = RB.Phi1'*D2*RB.Phi1;
    RB.offline_time = offline_time;
    RB.LB = 3;
    RB.UB = 1;
    RB.LUB = 1;
    par.RB = RB; 
%% ========================================================================
% Run RB solver vor various parameters in RB space with dimension k
%========================================================================
j = 1;
tsnaps = 100;
for i = 1:size(rand_p{1},1)
    progress_bar(j,tsnaps,bar,'Running for various parameters: ');
    par.arr_bp(1) = rand_p{1}(i);
    par.arr_bp(2) = rand_p{2}(i);
    par.arr_bp(3) = rand_p{3}(i);
    par.arr_bp(4) = rand_p{4}(i);
    gpar = par;
    % compute RB solution
    tic 
    rb_solution = RB_optimal_solver(par); % Run RB optimal solver.
    rbtime = rbtime +toc;
    % compute full solution
    tic
    solution = optimal_solver(par); % FE solution (in order to compute error)
    fetime = fetime +toc;
    j = j+1;
end
j1 = j1+1;
% close the waitbar figure
delete(bar);
%% ========================================================================
% Plot the error bounds
%========================================================================
cprintf('*Magenta','Average SpeedUp over Test Samples is: %3.2f \n',fetime/rbtime);
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);


function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);