function ex_RBoptim_pDose_constMat_MC
%   Runs Monte Carlo for an example for random parameters with normal 
% distributionin RB space by loading generated basis functions. 
%========================================================================
% Output: 
% - Computes optimal source for various parameters. Outputs average of
% them.
%
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha
% - Mean and Variance for normal distribution
%% ========================================================================
% Author(s)
%========================================================================
% Created on 05.05.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat-MC', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 100,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'sample_numb',10^2,... % number of sample parameters
'pvar',[0.01] , ... % interval position variance from the mean 
'lvar',[0] , ... % interval length variance from the mean 
'arr_p', [0.2 0.1 0.8 0.1],... % param. values for reference geometry
'arr_bp',[0.2 0.1 0.8 0.1],...  % param. values for actual geometry
'p_l',[0.15 0.05 0.75 0.05],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initizalization
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
des_dose = dose(par.x,par);
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
backpath = pwd;
cd ../RBbasis
par = RBload(par.name,par); % load basis functions and projected operators
cprintf('*Magenta','RB Basis functions are found and loaded!\n');
cd(backpath);
%% ========================================================================
% Run MC for 10^5 parameters
%========================================================================
%initialize progress bar
bar = progress_bar;
tot = par.sample_numb; 
for k = 1:size(par.pvar,2)
    p1 = normrnd(par.arr_p(1),par.pvar(k),[1 tot]);
    p2 = normrnd(par.arr_p(2),par.lvar(k),[1 tot]);
    p3 = normrnd(par.arr_p(3),par.pvar(k),[1 tot]);
    p4 = normrnd(par.arr_p(4),par.lvar(k),[1 tot]);    
    for l = 1:tot
        par.arr_bp = par.arr_p;
        par.arr_bp(1) = p1(l);
        par.arr_bp(2) = p2(l);
        par.arr_bp(3) = p3(l);
        par.arr_bp(4) = p4(l);
        output = RB_optimal_solver(par);
        sour(:,l) = output.Q;
        psi(:,l) = output.U;
        progress_bar(l,tot,bar,'Running for various parameters: ');
    end
    mean_sour(:,k) = mean(sour,2);
    mean_dose(:,k) = mean(psi,2);
end
delete(bar); % close progress bar
save('mean_p13_short_a=100','mean_sour','mean_dose');
%========================================================================
% Mean Source
%========================================================================
par.arr_bp = par.arr_p;
output = RB_optimal_solver(par);
mean_source = output.Q; 
mean_psi = output.U;
%========================================================================
% Distance from the Mean Source
%========================================================================
for i = 1:size(par.pvar,2)
    a = mean_source-mean_sour(:,i);
    b = mean_psi - mean_dose(:,i);
    l2_control(i) = norm(a,2);
    linf_control(i) = norm(a,inf);
    l2_dose(i) = norm(b,2);
    linf_dose(i) = norm(b,inf);
end
%========================================================================
% Plot mean of optimal control 
%========================================================================
figure;
plot(par.x,mean_source,'r--');
for i = 1:size(par.pvar,2)
    hold on; plot(par.x,mean_sour(:,i));
end
% legend('\sigma^2 = 0','\sigma^2 = 0.02','\sigma^2 = 0.03', ...
%     '\sigma^2 = 0.04','\sigma^2 = 0.05');
xlabel('x');
ylabel('q');
% Save the plot
if par.flag.save_plot
    cd ../Plots
    print('-depsc','mean_control_p13_multi_var_a=');
    cd(backpath);
end
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
sc = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) ... 
    / (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
a = (par.arr_p(1)+par.arr_p(2))+0.01;
b = (par.arr_p(3)-par.arr_p(4))-0.01;
f = sc*0.2*trap(x,[0.2 0.40 0.58 0.8]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
