function ex_RBoptim_pDose_constMat_generate_RBspace 
%   Takes the snapshots for various paramter values and generate
%  RB space using POD method. 
%
% Output: 
% - Projects full metrices to the RB space
% - Saves profected metrices in .mat file in folder ../RBbasis
%
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 29.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat-MC', ... % name for the example
'n', 200,... % number of points in spatial domain
'ax', [0 1], ... % spatial domain
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
backpath = pwd;
global gpar
bar = progress_bar;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
%% ========================================================================
% Initialization
%========================================================================
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);

% numsol = zeros(par.n+1,10);
j=1;tic
offline_time = 0;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
             progress_bar(j,tsnaps,bar,'Taking snapshots: ');
             par.arr_bp(1) = i;
             par.arr_bp(2) = k;
             par.arr_bp(3) = l;
             par.arr_bp(4) = m;
             gpar = par;
             solution = optimal_solver(par);      % Run optimal solver.
             q1(1:size(solution.Q,1),j) = solution.Q;
             psi1(1:size(solution.U{1},1),j) = solution.U{1};
             psi2(1:size(solution.U{2},1),j) = solution.U{2};
             adjoint1(1:size(solution.Lambda{1},1),j) = solution.Lambda{1};
             adjoint2(1:size(solution.Lambda{2},1),j) = solution.Lambda{2}; 
             offline_time = offline_time+sum(solution.cputime);
             j = j+1;
            end
        end
    end
end
delete(bar)% close progress bar
gtime = toc;
%% ========================================================================
% Compute weighting matrix W for integration for 1D
% use simple midpoint quadrature rule
%========================================================================
W = par.dx*ones(1,par.n+1);
W (1) = 0.5 * par.dx; %boundary
W(end) = 0.5*par.dx; %boundary
inProd = diag(W);
%% ========================================================================
% Generate Basis functions in RB space and save them
%========================================================================
 z1 = [psi1 psi2 adjoint1 adjoint2];% basis for psi and adjoints
 z2 = q1; % basis for source term
 Phi2 = generate_PODbasis(z2,1e-8);
 M = size(Phi2,2);
 Phi1 = generate_PODbasis(z1,1e-8,2*M); % generate b.f. for 0th moment state
 % generate full metrices
 multS = gen_multS_matrices(par);
 if par.flag.material % if mat.coef are piecewise constant
    multsT1 = gen_multi_sigma_matrixa(par,0);
    multsT2 = gen_multi_sigma_matrixa(par,1);
   else % if mat.coef are spatial dependent
    multsT1 = gen_multi_sigma_matrix(par,0);
    multsT2 = gen_multi_sigma_matrix(par,1);
 end
 Br = gen_br_matrix(par.n+1); % values for bourdary elements
 D1 = gen_d1matrix(par.n+1); % matrix at transport operator
 D2 = -D1';
 for i = 1:size(multS,2)
     multSx{i} = Phi1'*multS{i}*Phi1;
     multSy{i} = Phi2'*multS{i}*Phi2;
     multSd{i} = Phi1'*multS{i};
     multS{i} = Phi1'*multS{i}*Phi2;
     sT1{i} = Phi1'*multsT1{i}*Phi1;
     sT2{i} = Phi1'*multsT2{i}*Phi1;
 end
 Br = Phi1'*Br*Phi1;
 D1 = Phi1'*D1*Phi1;
 D2 = Phi1'*D2*Phi1;
 % I do not need the for the moment
 LB = 3;
 UB = 1;
 LUB = 1;
 % save reduced metrices
 cd ../RBbasis
 save([par.name '.mat'],'Phi1','Phi2','multS','multSx','multSy','multSd','sT1','sT2', ...
     'Br','D1','D2','offline_time','LB','UB','LUB'); % save basis functions
 cd(backpath); % go back to the folder with the example
 cprintf('*Magenta','RB Basis functions for "%s" are generated!\n',par.name);
 cprintf('*Blue','Computational time: %3.4fs\n',gtime);
 %% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = dose(x,par)
% desired dose function
sc = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) ... 
    / (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
a = (par.arr_p(1)+par.arr_p(2))+0.01;
b = (par.arr_p(3)-par.arr_p(4))-0.01;
f = sc*0.2*trap(x,[0.36 0.44 0.56 0.64]);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
