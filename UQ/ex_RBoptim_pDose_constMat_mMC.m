function ex_RBoptim_pDose_constMat_mMC
%   Runs Monte Carlo for an example for random parameters with normal 
% distributionin RB space by loading generated basis functions. Changes
% both interval size and their positions
%========================================================================
% Output: 
% - Computes optimal source for various parameters. Outputs average of
% them.
%
%========================================================================
% Input: 
% - Desired dose: 
%   (1) is parametrized for geometry 
%   (2) non-zero in 3rd region
%   (3) zero in all other regions
% - Material parameters are given as piecewise constant function 
% - Regularization parameter alpha
% - Mean and Variance for normal distribution
%% ========================================================================
% Author(s)
%========================================================================
% Created on 05.05.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex-RBoptim-pDose-constMat-MC', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'alpha',1e1, ... % regularization parameter as constant
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'dose', @dose,... % desired dose function
'mom_output',[1 2],... % output moments 0th, 1st or both
'sample_numb',10^5,... % number of sample parameters
'pvar',[0.02 0.03 0.04 0.05], ... % interval position variance from the mean 
'lvar',[0.02 0.03 0.04 0.05], ... % interval length variance from the mean 
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',1, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initizalization
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
des_dose = dose(par.x,par);
ex = linspace(0,1,500);
%% ========================================================================
% Initialization
%========================================================================
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
backpath = pwd;
cd ../RBbasis
par = RBload(par.name,par); % load basis functions and projected operators
cprintf('*Magenta','RB Basis functions are found and loaded!\n');
cd(backpath);
%% ========================================================================
% Run MC for 10^5 parameters
%========================================================================
% initialize progress bars
bar = MultiWaitBar(2, 1, 'Progress Bar', 'g'); 
barName = {'Variance', 'Various parameters'};
for ix = 1:2 % initialize waitbars
   bar.Update(ix, 1, 0, barName{ix});
end
tot = par.sample_numb; 
M = size(par.pvar,2);
for k = 1:size(par.pvar,2)
    bar.Update(1, 1, k/M, ...
        sprintf('Random parameters with variance %1.3f ',par.pvar(k)), ...
        rgb2hsv([k/M, 1, 1]));
    p1 = normrnd(par.arr_p(1),par.pvar(k),[1 tot]);
    p2 = normrnd(par.arr_p(2),par.lvar(k),[1 tot]);
    p3 = normrnd(par.arr_p(3),par.pvar(k),[1 tot]);
    p4 = normrnd(par.arr_p(4),par.lvar(k),[1 tot]);
    jp = 1; % number of admissible parameters
    ip = 1; % number of non-admissible parameters
    for l = 1:tot
        bar.Update(2, 1, l/tot,sprintf('Running for various parameters: %d%%',floor(l*100/tot)),...
        rgb2hsv(([l/tot, 1, 1])));
        par.arr_bp = par.arr_p;
        par.arr_bp(1) = p1(l);
        par.arr_bp(2) = p2(l);
        par.arr_bp(3) = p3(l);
        par.arr_bp(4) = p4(l);
        if pCheck(p1(l),p2(l),p3(l),p4(l)) % check p.for admissibility
            output = RB_optimal_solver(par);
            % generate discretization points of the original geometry
            ox = disc(par);
            % interpolate the source
            sour(:,jp) = interp1(ox,output.Q,ex); 
            % interpolate the state
            psi(:,jp) = interp1(ox,output.U,ex); 
            jp = jp+1;
        else
            ip = ip +1;
        end
    end
    mean_sour(:,k) = mean(sour,2);
    mean_dose(:,k) = mean(psi,2);
end
bar.Close(); % close progress bar
cprintf('*Blue','Number of sample parameters: %d \n',jp-1);
cprintf('*Blue','Number of non-admissible parameters: %d\n',ip-1); 
save('mean_p1234_a=10','mean_sour','mean_dose');
%========================================================================
% Mean Source
%========================================================================
par.arr_bp = par.arr_p;
output = RB_optimal_solver(par);
ox = disc(par);
mean_source = interp1(ox,output.Q,linspace(0,1,500)); 
mean_psi = interp1(ox,output.U,linspace(0,1,500));
%========================================================================
% Distance from the Mean Source
%========================================================================
for i = 1:size(par.pvar,2)
    a = mean_source-mean_sour(:,i)';
    b = mean_psi - mean_dose(:,i)';
    l2_control(i) = norm(a,2)/norm(mean_sour,2);
    linf_control(i) = norm(a,inf)/norm(mean_sour,inf);
    l2_dose(i) = norm(b,2);
    linf_dose(i) = norm(b,inf);
end
%========================================================================
% Plot mean of optimal control 
%========================================================================
figure;
plot(ex,mean_source,'r-','LineWidth',1.3);
pStyle = {'b-.', 'k--','g--','m-.', 'b--','k-.'};
for i = 3:size(par.pvar,2)
    hold on; plot(ex,mean_sour(:,i),pStyle{i-2},'LineWidth',1.3);
end
legend('Mean', ...
     'Var = 0.04','Var = 0.05','Location','northwest');
xlabel('x');
ylabel('Average Optimal Source');
set(gca,'FontSize',19);
% Save the plot
if par.flag.save_plot
    cd ../Plots/UQ
    print('-depsc','mean_control_p1234');
    matlab2tikz('control_p1234.tikz', 'height', '\figureheight', 'width', '\figurewidth'); 
    cd(backpath);
end
%========================================================================
% Plot distance from the mean control
%========================================================================
figure; 
plot([0.02 0.03 0.04 0.05],l2_dose,'r*-');
hold on;
plot([0.02 0.03 0.04 0.05],linf_dose,'b*-.');
legend('L-2 norm', 'L-inf norm','Location','northwest');
xlabel('Variance');
ylabel('Difference from the mean control');
set(gca,'FontSize',19);
if par.flag.save_plot
    cd ../Plots/UQ
    print('-depsc','distance_mean_p1234');
    matlab2tikz('error_p1234.tikz', 'height', '\figureheight', 'width', '\figurewidth'); 
    cd(backpath);
end
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

% function f = dose(x,par)
% % desired dose function
% sc = (par.arr_p(3)-par.arr_p(4)-par.arr_p(1)-par.arr_p(2)) ... 
%     / (par.arr_bp(3)-par.arr_bp(4)-par.arr_bp(1)-par.arr_bp(2));
% a = (par.arr_p(1)+par.arr_p(2))+0.01;
% b = (par.arr_p(3)-par.arr_p(4))-0.01;
% f = sc*0.2*trap(x,[0.36 0.44 0.56 0.64]);

function f = dose(x,par)
% desired dose function
f = 0.2*trap(x,[0.38 0.45 0.55 0.62]);


function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
