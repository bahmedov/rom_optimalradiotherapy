function ex_RBforw_constMat_mLowerBound
% - Generates RB space with various dimension N (e.g.N = 1,2,3,...). 
%
% - Runs reduced solver and finds lower bound (LB) of the transport
% operator
%
% - Finds minimum (LB) over tested parameters p for each N.
%========================================================================
% Input: 
% -  Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)
% -  Material parameters are given as piecewise constant function 
%========================================================================
% Output: 
% - Plot minimum LB over various parameters p for each N
%% ========================================================================
% Author(s)
%========================================================================
% Created on 02.05.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
backpath = pwd;
cputime = zeros(1,4); % cputime
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
bar = progress_bar; % initialize progress bar
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Taking snapshots
%========================================================================
par.flag.evaLB = 1;
j=1;tic
offline_time = 0;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
                progress_bar(j,tsnaps,bar,'Taking snapshots: ');
                par.arr_bp(1) = i;
                par.arr_bp(2) = k;
                par.arr_bp(3) = l;
                par.arr_bp(4) = m;
                gpar = par;
                solution = solver(par);                     % Run solver.
                offline_time = offline_time+sum(solution.cputime);
                S1(:,j) = solution.U{1};
                S2(:,j) = solution.U{2};
                LB(j) = solution.LB; 
                j = j+1;
            end
        end
    end
end
delete(bar)% close progress bar
gtime = toc;
%% ========================================================================
% Generate Basis functions in RB space
%========================================================================
 Phi1 = generate_PODbasis([S1 S2],1e-8,par.n); % generate b.f. for 0th moment 
 Phi2 = Phi1;
 M = size(Phi1,2); % size of RB space
 cprintf('*Magenta','RB Basis is constructed. \n');
%% ========================================================================
% Check the min LB for the RB space with dimension k
%========================================================================
k = 15; % check the min lB for the RB space with dimension k
par.flag.evaRB_LB = 1;
bar = progress_bar; % initialize progress bar
clear RB S1 S1_rb S2_rb
RB.Phi1 = Phi1(:,1:k);
RB.Phi2 = Phi2(:,1:k);
% generate full metrices
multS = gen_multS_matrices(par);
multsT1 = gen_multi_sigma_matrices(par,0);
multsT2 = gen_multi_sigma_matrices(par,1);
Br = gen_br_matrix(par.n+1); % values for bourdary elements
D1 = gen_d1matrix(par.n+1); % matrix at transport operator
D2 = -D1';
for i = 1:size(multS,2)
    RB.multS{i} = RB.Phi1'*multS{i};
    RB.multsT1{i} = RB.Phi1'*multsT1{i}*RB.Phi1;
    RB.multsT2{i} = RB.Phi2'*multsT2{i}*RB.Phi2;
end
RB.Br = RB.Phi1'*Br*RB.Phi1;
RB.D1 = RB.Phi2'*D1*RB.Phi1;
RB.D2 = RB.Phi1'*D2*RB.Phi2;
RB.offline_time = offline_time;
par.RB = RB; 
%% ========================================================================
% Run RB solver vor various parameters in RB space with dimension k
%========================================================================
j=1;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
                progress_bar(j,tsnaps,bar,'Taking snapshots: ');
                par.arr_bp(1) = i;
                par.arr_bp(2) = k;
                par.arr_bp(3) = l;
                par.arr_bp(4) = m;
                gpar = par;
                rb_sol = RB_solver(par);
                RB_LB(j) = rb_sol.RB_LB;  
                j = j+1;
            end
        end
    end
end
minRBLB = min(RB_LB);
cputime(3) = par.RB.offline_time;
print_cputime(par,cputime);
delete(bar); %close the progress bar
save('minlb.mat','minLB') % save the values of the lower bounds
%% ========================================================================
% Plot : minimu lower bounds for each N
%========================================================================
 % plot over number of used bases for max.error over all parameter
font_size = 21;
figure;
semilogy(1:1:M,minLB,'b*--');
ylim([3 4]);
grid on;
hold on;
xlabel('$\mathcal{N}$', 'Interpreter', 'latex');
title('Minimum inf-sup $\beta_{LB}$','Interpreter','latex');
set(gca,'FontSize',font_size)
set(gca,'XGrid','on');
set(gca,'YGrid','on');
set(gca, 'yminorgrid', 'off') % only plot major grid lines
ax = gca;
ax.Title.FontWeight = 'normal';

font_size = 21;
figure;
semilogy(1:1:size(LB,2),LB,'b*--');
grid on;
hold on;
xlabel('random parameters', 'Interpreter', 'latex');
title('Inf-sup $\beta_{LB}$ for $\mathcal{N}=$25','Interpreter','latex');
set(gca,'FontSize',font_size)
set(gca,'XGrid','on');
set(gca,'YGrid','on');
set(gca, 'yminorgrid', 'off') % only plot major grid lines
ax = gca;
ax.Title.FontWeight = 'normal';
print('-depsc','RBforw-mLB_25');
%% ========================================================================
% Save the plots
%========================================================================
if par.flag.save_plot 
    cd ../Plots/RBforw
        print('-depsc','RBforw-constMat-mLB');
    cd(backpath); % go back to the original folder
end
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);