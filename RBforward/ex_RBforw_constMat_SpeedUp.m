function ex_RBforw_constMat_SpeedUp
% -  Checks speed-up of the RB solver with compare to the full FE solution
% - Runs FE solution for a test sample and take average time as 
% a FE time to compare
% - Runs RB solution for a test sample and take average time as 
% a RB time to compare
%
% - Runs reduced solver for multiple parameters. Each parameter is chosen
% from indicated range. 
%
% -   Finds maximum error over tested parameters for each N. Computes error
% estimator and plots is in the same figure with "truth" error.
%   
% -  Load RB space, which is generated in advance. Note, that par.name
% should be the same as in generating .m file. 
%
% -  Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 15.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],... % param. values for reference geometry
'arr_bp',[0.2 0.07 0.8 0.07],...  % param. values for actual geometry
'p_l',[0.15 0.04 0.75 0.04],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',1, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
backpath = pwd;
cputime = zeros(1,4); % cputime
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
bar = progress_bar; % initialize progress bar
%% ========================================================================
% Initialization
%========================================================================
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Taking snapshots
%========================================================================
par.flag.evaLB = 0;
par.flag.evaUB = 0;
minLB = 3;
maxUB = 1;
j=1;tic
offline_time = 0;
tsnaps = 6*3*6*3; % total number of snapshots
for i = 0.15:0.02:0.25
    for k = 0.04:0.03:0.1
        for l = 0.75:0.02:0.85
            for m = 0.04:0.03:0.1
                progress_bar(j,tsnaps,bar,'Taking snapshots: ');
                par.arr_bp(1) = i;
                par.arr_bp(2) = k;
                par.arr_bp(3) = l;
                par.arr_bp(4) = m;
                gpar = par;
                solution = solver(par);                     % Run solver.
                offline_time = offline_time+sum(solution.cputime);
                S1(:,j) = solution.U{1};
                S2(:,j) = solution.U{2};
                j = j+1;
            end
        end
    end
end
delete(bar)% close progress bar
gtime = toc;
%% ========================================================================
% Compute weighting matrix W for integration for 1D
% use simple midpoint quadrature rule
%========================================================================
W = par.dx*ones(1,par.n+1);
W (1) = 0.5 * par.dx; %boundary
W(end) = 0.5*par.dx; %boundary
inProd = diag(W);
%% ========================================================================
% Generate Basis functions in RB space
%========================================================================
 Phi1 = generate_PODbasis([S1 S2],1e-10); % generate b.f. for 0th moment 
 Phi2 = Phi1;
 M = size(Phi1,2); % size of RB space
 cprintf('*Magenta','RB Basis is constructed. \n');
%% ========================================================================
% Initialization
%========================================================================
par.flag.evaLB = 0;
par.flag.evaUB = 0;
par.flag.evaRes = 0;
X = gen_xnorm(par.n+1,par.dx); % inner products metrics
% initialize progress bar
bar = progress_bar; % initialize progress bar
% test parameter values
N1 = 100; N2 = 100;
tsnaps = 100; % total number of snapshots
rand_p{1} = par.p_l(1) + (par.p_u(1)-par.p_l(1)).*rand(N1,1);
rand_p{2} = par.p_l(2) + (par.p_u(2)-par.p_l(2)).*rand(N2,1);
rand_p{3} = par.p_l(3) + (par.p_u(3)-par.p_l(3)).*rand(N1,1);
rand_p{4} = par.p_l(4) + (par.p_u(4)-par.p_l(4)).*rand(N2,1);
nd = 5; % number of tested dimension
j1 = 1; % 
%% ========================================================================
% Loop for various RB dimension
%========================================================================
    k = 35;
    clear RB S1 S1_rb S2_rb
    RB.Phi1 = Phi1(:,1:k);
    RB.Phi2 = Phi2(:,1:k);
    % generate full metrices
    multS = gen_multS_matrices(par);
    multsT1 = gen_multi_sigma_matrices(par,0);
    multsT2 = gen_multi_sigma_matrices(par,1);
    Br = gen_br_matrix(par.n+1); % values for bourdary elements
    D1 = gen_d1matrix(par.n+1); % matrix at transport operator
    D2 = -D1';
    for i = 1:size(multS,2)
        RB.multS{i} = RB.Phi1'*multS{i};
        RB.multsT1{i} = RB.Phi1'*multsT1{i}*RB.Phi1;
        RB.multsT2{i} = RB.Phi2'*multsT2{i}*RB.Phi2;
    end
    RB.Br = RB.Phi1'*Br*RB.Phi1;
    RB.D1 = RB.Phi2'*D1*RB.Phi1;
    RB.D2 = RB.Phi1'*D2*RB.Phi2;
    RB.offline_time = offline_time;
    RB.LB = minLB;
    RB.UB = maxUB;
    par.RB = RB; 
%% ========================================================================
% Run RB solver vor various parameters in RB space with dimension k
%========================================================================
j = 1;
rbtime = 0; % full time for the RB solution 
fetime = 0; % full time for the FE solution
for i = 1:size(rand_p{1},1)
    progress_bar(j,tsnaps,bar,'Running for various parameters: ');
    par.arr_bp(1) = rand_p{1}(i);
    par.arr_bp(2) = rand_p{2}(i);
    par.arr_bp(3) = rand_p{3}(i);
    par.arr_bp(4) = rand_p{4}(i);
    gpar = par;
    % compute RB solution
    tic
    rb_sol = RB_solver(par);
    rbtime = rbtime + toc;
    % compute full solution
    tic
    S1 = solver(par);
    fetime = fetime+toc;
    j = j+1;
end
 cputime(3) = par.RB.offline_time;
 print_cputime(par,cputime);
 delete(bar);
 cprintf('*Magenta','Average SpeedUp over Test Samples is: %f \n',fetime/rbtime);
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);