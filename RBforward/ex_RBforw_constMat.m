function ex_RBforw_constMat
% - Runs RB solver by loading generated basis functions. 
% - Runs full solver for the same test case.
% - Plots the results for both solvers.
% - Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)  
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 15.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.1 0.8 0.1],... % param. values for reference geometry
'arr_bp',[0.23 0.07 0.81 0.07]...  % param. values for actual geometry
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',1, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',1 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
 global gpar
 gpar = par;
 par.dx = (par.ax(2)-par.ax(1))/par.n; 
 par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
 % generate solution metrices
 par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
 par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
 par.FE.D2 = -par.FE.D1';
 par.FE.multS = gen_multS_matrices(par);
 par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
 par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Run FE solver
%========================================================================
 % Full-forward solution
 tic
 full_sol = solver(par);                                % Run solver.
 S1 = full_sol.U{1};
 S2 = full_sol.U{2};
 forw = toc;
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
 backpath = pwd;
 cd ../RBbasis
 par = RBload(par.name,par); % load basis functions and projected operators
 cd(backpath);
%% ========================================================================
% Run RB solver
%========================================================================
 cputime = zeros(1,4);
 rb_sol = RB_solver(par);
 cputime = cputime + rb_sol.cputime;
 S1_rb = rb_sol.U{1};
 S2_rb = rb_sol.U{2};
 cputime(3) = par.RB.offline_time;
 print_cputime(par,cputime);
 cprintf('Black',' Full FE solution:  %2.2es \n',forw);
 cprintf('Black',' RB solution:  %13.2es \n',cputime(2));
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
