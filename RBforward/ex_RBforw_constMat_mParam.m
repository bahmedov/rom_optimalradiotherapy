function ex_RBforw_constMat_mParam
%    Runs reduced solver for multiple parameters. Each parameter is chosen
% from indicated range. Compares the solution with "truth" solution for
% each parameter and plots L2 difference btw RB and "truth" solution. 
%   
%   Load RB space, which is generated in advance. Note, that par.name
% should be the same as in generating .m file. 
%
% -  Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 15.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%% ========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'cputime',zeros(1,4),... % cputime
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'offline_time',0,... % CPU time for offline computations
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.1 0.8 0.1],... % param. values for reference geometry
'arr_bp',[0.2 0.1 0.8 0.1],...  % param. values for actual geometry
'p_l',[0.15 0.05 0.75 0.05],... % lower range for the parameters
'p_u',[0.25 0.1 0.85 0.1]...    % upper range for the parameters
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Moment System Setup and Solver Execution
%========================================================================
global gpar
gpar = par;
par.dx = (par.ax(2)-par.ax(1))/par.n; 
par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
% generate solution metrices
par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
par.FE.D2 = -par.FE.D1';
par.FE.multS = gen_multS_matrices(par);
par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
%% ========================================================================
% Load low-dimensional spaces, and basis functions
%========================================================================
 backpath = pwd;
 cd ../RBbasis
 par = RBload(par.name,par); % load basis functions and projected operators
 cd(backpath); 
%% ========================================================================
% Initialization
%========================================================================
cputime = zeros(1,4);
par.flag.evaRes = 1;
par.flag.evaLB = 1;
X = gen_xnorm(par.n+1,par.dx);
j = 1;
bar = progress_bar; % initialize progress bar
tsnaps = 100; % number of test parameters
N1 = 100; N2 = 100;
%% ========================================================================
% RB solution
%========================================================================
rand_p{1} = par.p_l(1) + (par.p_u(1)-par.p_l(1)).*rand(N1,1);
rand_p{2} = par.p_l(2) + (par.p_u(2)-par.p_l(2)).*rand(N2,1);
rand_p{3} = par.p_l(3) + (par.p_u(3)-par.p_l(3)).*rand(N1,1);
rand_p{4} = par.p_l(4) + (par.p_u(4)-par.p_l(4)).*rand(N2,1);
for i = 1:size(rand_p{1},1)
    progress_bar(j,tsnaps,bar,'Running RB solver for various parameters:');
    par.arr_bp(1) = rand_p{1}(i);
    par.arr_bp(2) = rand_p{2}(i);
    par.arr_bp(3) = rand_p{3}(i);
    par.arr_bp(4) = rand_p{4}(i);
    gpar = par;
    % compute RB solution
    rb_sol = RB_solver(par);
    cputime = cputime + rb_sol.cputime;
    S1_rb(:,j) = rb_sol.U{1};
    S2_rb(:,j) = rb_sol.U{2};
    par.sol_RB = rb_sol.U{1};
    % compute full solution
    S1 = solver(par);
    EB(j) = S1.nRes/S1.LB;
    l2_error(j) = sqrt((S1.U{1}-rb_sol.U{1})'*X* ...
        (S1.U{1}-rb_sol.U{1}))/sqrt(S1.U{1}'*X*S1.U{1});
%     LB(j) = solution.RB_LB;
    j = j+1;
end
 cputime(3) = par.RB.offline_time;
 print_cputime(par,cputime);
 delete(bar);
%% ========================================================================
% L2-different btw the "truth" and RB solution for each parameter 
%========================================================================
font_size = 15;
figure; 
semilogy(l2_error);
xlabel('Various parameters')
ylabel('Relative L_2 error');
set(gca,'FontSize',font_size)
% maximum error over parameters
cprintf('*Magenta','Maximum error over parameters is: %2.2e \n',max(l2_error));
cprintf('*Magenta','Number of tested parameters %d\n',tsnaps);
cprintf('*Magenta','Dimension of RB space %d\n',size(par.RB.Phi1,2));
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);