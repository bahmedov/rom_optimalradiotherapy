function ex_RBforw_constMat_generate_RBspace
%   Takes the snapshots for various paramter values and generate
%  RB space using POD method. 
% 
%  Source term is given as: pi^2/3*sin(pi*x)+sin(pi*x)
% - Material parameters are given as piecewise constant function 
%% ========================================================================
% Author(s)
%========================================================================
% Created on 15.04.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |

%========================================================================
% Problem Parameters
%========================================================================
par = struct(...
'name', 'ex_RBforw_constMat', ... % name for the example
'n', 200,... % number of points in spatial domain
'ax', [0 1], ...
'sigma_a',@sigma_a,... % absorption coefficient (defined below)
'sigma_s0',@sigma_s0,... % isotropic scattering coefficient (def. below)
'sigma_sm',@sigma_sm,... % % aniso. scattering coefficient (defined below)
'source',@source,... % source function
'mom_output',[1 2],... % output moments 0th, 1st or both
'arr_p', [0.2 0.07 0.8 0.07],...
'arr_bp',[0.2 0.07 0.8 0.07]...
);
%% ========================================================================
% Flags
%========================================================================
par.flag = struct(...
'material',1,... % 1 - piecewise constant mat.par., 0 - function mat.par.     
'ifplot',0, ... % if it is not necessary to print the solution 
'evaLB',0, ... % evaluate lower bound of the bilinear operator
'evaRes',0, ... % evaluate residual: RBsolution - RHS
'save_plot',0, ... % save the plot 
'ifdisc_s',0,... % if source is discrete
'half_plot',0 ... % half plot for comparison with analytical solution
);
%% ========================================================================
% Initialization
%========================================================================
 global gpar
 bar = progress_bar; 
 par.dx = (par.ax(2)-par.ax(1))/par.n; 
 par.x = linspace(par.ax(1),par.ax(2),par.n+1); % # of points
 % generate solution metrices
 par.FE.Br = gen_br_matrix(par.n+1); % values for bourdary elements
 par.FE.D1 = gen_d1matrix(par.n+1); % matrix at transport operator
 par.FE.D2 = -par.FE.D1';
 par.FE.multS = gen_multS_matrices(par);
 par.FE.multsT1 = gen_multi_sigma_matrices(par,0);
 par.FE.multsT2 = gen_multi_sigma_matrices(par,1);
% numsol = zeros(par.n+1,10);
%% ========================================================================
% taking the snapshots
%========================================================================
 % set the flags
par.flag.evaLB = 1;
par.flag.evaUB = 1;
j=1;tic
offline_time = 0;
tsnaps = 5*5*5*5; % total number of snapshots
for i = 0.15:0.025:0.25
    for k = 0.04:0.015:0.1
        for l = 0.75:0.025:0.85
            for m = 0.04:0.015:0.1
                progress_bar(j,tsnaps,bar,'Taking snapshots: ');
                par.arr_bp(1) = i;
                par.arr_bp(2) = k;
                par.arr_bp(3) = l;
                par.arr_bp(4) = m;
                gpar = par;
                solution = solver(par);                     % Run solver.
                offline_time = offline_time+sum(solution.cputime);
                S1(:,j) = solution.U{1};
                S2(:,j) = solution.U{2};
                mLB (j) = solution.LB;
                mUB (j) = solution.UB;
                j = j+1;
            end
        end
    end
end
LB = min(mLB);
UB = min(mUB);
delete(bar)% close progress bar
gtime = toc;
%% ========================================================================
% Compute weighting matrix W for integration for 1D
% use simple midpoint quadrature rule
%========================================================================
W = par.dx*ones(1,par.n+1);
W (1) = 0.5 * par.dx; %boundary
W(end) = 0.5*par.dx; %boundary
inProd = diag(W);
%% ========================================================================
% Generate Basis functions in RB space and save them
%========================================================================
 Phi1 = generate_PODbasis([S1 S2],1e-10); % generate b.f. for 0th moment 
 Phi2 = Phi1;
 M = min(size(Phi1,2),size(Phi2,2));
 Phi1 = Phi1(:,1:M);
 Phi2 = Phi2(:,1:M);
 % generate full metrices
 multS = gen_multS_matrices(par);
 multsT1 = gen_multi_sigma_matrices(par,0);
 multsT2 = gen_multi_sigma_matrices(par,1);
 Br = gen_br_matrix(par.n+1); % values for bourdary elements
 D1 = gen_d1matrix(par.n+1); % matrix at transport operator
 D2 = -D1';
 for i = 1:size(multS,2)
     multS{i} = Phi1'*multS{i};
     sT1{i} = Phi1'*multsT1{i}*Phi1;
     sT2{i} = Phi2'*multsT2{i}*Phi2;
 end
 Br = Phi1'*Br*Phi1;
 D1 = Phi2'*D1*Phi1;
 D2 = Phi1'*D2*Phi2;
 % save reduced metrices
 cd ../RBbasis
 save([par.name '.mat'],'Phi1','Phi2','multS','sT1','sT2', ...
     'Br','D1','D2','LB','UB','offline_time'); % save basis functions
 cd ../RBForward % go back to the folder with the example
 cprintf('*Magenta','RB Basis functions for "%s" are generated!\n',par.name);
 cprintf('*Blue','Computational time: %3.4fs\n',gtime);
%% ========================================================================
% Problem Specific Functions
%========================================================================
function f = sigma_a(x)
global gpar
% Absorption coefficient.
f = [1 2 1 2 1];
f = f.*3;

function f = sigma_s0(x)
global gpar
% Total scattering coefficient.
f = [1 2 1 2 1];

function f = sigma_sm(x,m)
global gpar
% Moments of scattering kernel
f = sigma_s0(x).*(m==0);

function f = source (x,l)
global gpar
% source term
f = pi^2/3*sin(pi*x)'+sin(pi*x)';
f = f';
f = f*(l==0);
