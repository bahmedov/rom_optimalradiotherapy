%% ========================================================================
% Author(s)
%========================================================================
% Created on 09.12.2015 by Bahodir Ahmedov. 
%==========================================================================
% Date     |     Author(s)           |         Notes    
%==========================================================================
%          |                         |
%          |                         |
%% ========================================================================
% Description
%========================================================================
% Initialize all the paths. Sets and adds new paths. 

current_path = pwd; % current folder
func_path = [pwd '/Functions']; % folder with all functions
solver_path = [pwd '/Solvers']; % folder with all solvers
addpath(func_path); % add folder with Functions to the search path
addpath(solver_path); %add folder with solvers to the search path